import React, {Fragment} from 'react';
import '../scss/modal.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons'

export default class Modal extends React.Component{

    render() {

        return <div className='modal' onClick={this.props.clickHandler}>
            <div className='modal__content'
                 onClick={e=> e.stopPropagation()}
            >
                <div className='modal__content__header'>
                    <h3>{this.props.header}</h3>
                    <span className='modal__content__close-btn' onClick={this.props.clickHandler}>{this.props.closeButton && <FontAwesomeIcon icon={faXmark}/>}</span>
                </div>
                <div className='modal__content__body'>
                    <p>{this.props.text}</p>
                </div>

                <div className='modal__content__footer'>
                    {this.props.action}
                </div>
            </div>

        </div>

    }
}