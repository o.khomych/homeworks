import React from 'react'
import '../scss/button.scss'
export default class Button extends React.Component{
    render(){
        return <button onClick={this.props.clickHandler}
                       style={{backgroundColor: this.props.backgroundColor}}
                       id={this.props.id}
                        className={this.props.className}>
            {this.props.text}
        </button>
    }
}