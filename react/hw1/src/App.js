import React from 'react'
import Button from "./components/Button";
import './scss/style.scss'
import Modal from "./components/Modal";

const modal= [
    {
        header: 'Do you want to delete this file?',
        text: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?',
        btnClassName: 'modal__content__footer__btn--var1',
        btnText1: 'OK',
        btnText2: 'Cancel'
    },
    {
        header:'Modal is open!',
        text: 'Please, choose color!',
        btnClassName: 'modal__content__footer__btn--var2',
        btnText1: 'Green',
        btnText2: 'Yellow'
    }
]
const btn = [{id:1, text:'Open first modal', bColor: 'purple'}, {id:2, text:'Open second modal', bColor: 'red'}]

export default class App extends React.Component{
    state = {
        isOpen: false,
        variant: ''
    }

    openModal = (e)=>{
        this.setState(()=>{
            return {isOpen: true,
                    variant: e.target.id - 1}
        })
    }

    closeModal =()=>{
        this.setState(()=>{
            return {isOpen: false}
        })
    }

    render(){
        return <div className={'app'} >
            {btn.map((item)=>{
                return <Button clickHandler={this.openModal} text={item.text} backgroundColor={item.bColor} key={item.id} id={item.id}/>
            })}
            {this.state.isOpen && <Modal
                header={modal[this.state.variant].header}
                closeButton={true}
                text={modal[this.state.variant].text}
                clickHandler={this.closeModal}
                action={
                <div  className='modal__content__footer__btn-wrap'>
                    <Button className={modal[this.state.variant].btnClassName} clickHandler={this.closeModal} text={modal[this.state.variant].btnText1}></Button>
                    <Button className={modal[this.state.variant].btnClassName} clickHandler={this.closeModal} text={modal[this.state.variant].btnText2}></Button>
                </div>}
            >
            </Modal>}
        </div>
    }
}
