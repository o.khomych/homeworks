import {rootReducer, initialState} from "./reducer";
import {hideModal, HideModalAction, ITEMS, loadItems, showModal, ShowModalAction} from "./action";
import data from '../items.json'

describe('reducer test', ()=>{
    test('should return the initial state', ()=>{
        const initialState = undefined
        const action = {type: undefined}
        const result = rootReducer(initialState, action)
        expect(result).toEqual({items: [],modal: false})
    })
    test('modal should be true', ()=>{
        const action = {
            type: ShowModalAction,
            payload: true
        }
        const result = rootReducer(initialState, action)
        expect(result).toEqual({items: [],modal: true})
    })
    test('modal should be false', ()=>{
        const action = {
            type: HideModalAction,
            payload: false
        }
        const result = rootReducer(initialState, action)
        expect(result).toEqual({items: [],modal: false})
    })
    test('convert data to an object',()=>{
        const initialState = undefined
        const action = {
            type: ITEMS,
            payload: data
        }
        const result = rootReducer(initialState, action)
        expect(result.items.length).toEqual(data.length);
    })
})
