import Button from "./index";
import renderer from 'react-test-renderer';
import {fireEvent, render, screen} from "@testing-library/react"
import React from 'react'
import '@testing-library/jest-dom'

test("Button snapshot",  () => {
    const tree = renderer.create('<Button clickHandler={jest.fn()} className="test" text="test"/>').toJSON();
    expect(tree).toMatchSnapshot();
})

describe('Button test', () => {
    test('Button render', ()=>{
        const view = render(<Button className="testClassName" text="button name"/>)
        expect(screen.getByRole('button')).toBeInTheDocument();
        expect(screen.getByText('button name')).toBeInTheDocument();
        expect(screen.getByText('button name')).toHaveClass('testClassName')
    })
    test('Button change color', () =>{
        const view = render(<Button backgroundColor='red' className="testClassName" text="button name"/>)
        expect(screen.getByTestId('background')).toHaveStyle('background-color: red');
    })

    test('Button should be click 1 time', ()=>{
        const handleClick = jest.fn()
        const view = render(<Button clickHandler={handleClick} className="testClassName" text="button name"/>)
        expect(screen.getByRole('button')).toBeInTheDocument();
        fireEvent.click(screen.getByRole('button'))
        expect(handleClick).toHaveBeenCalledTimes(1)
    })

    test('test clicking the button triggers the onSubmit function', ()=>{
        const onSubmit = jest.fn()
        const view = render(<Button clickHandler={onSubmit} type='submit' className="testClassName" text="button name"/>)
        expect(screen.getByRole('button')).toBeInTheDocument();
        fireEvent.click(screen.getByRole('button'))
        expect(onSubmit).toHaveBeenCalled()
    })
})