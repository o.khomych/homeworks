import React from 'react'
import './button.scss'
import propTypes from 'prop-types'

export default function Button (props){
        return <button role="button" onClick={props.clickHandler}
                       type={props.type}
                       style={{backgroundColor: props.backgroundColor}} data-testid='background'
                        className={props.className}>
            {props.text}
        </button>
}

Button.propTypes = {
    clickHandler: propTypes.func,
    text: propTypes.oneOfType([propTypes.string, propTypes.node, propTypes.element]).isRequired,
    backgroundColor: propTypes.string,
    className: propTypes.string,
    type: propTypes.oneOf(['button', 'submit', 'reset']),
}
Button.defaultProps ={
    backgroundColor: 'none',
    type: 'button'
}