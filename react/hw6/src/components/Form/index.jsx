import React from 'react'
import './chekout.scss'
import {getIn} from 'formik'
import {NumericFormat, PatternFormat} from 'react-number-format'

export default function Input (props){
    const {field, form, label, ...rest} = props;
    const {name} = field;
    function getStyles(errors, touched, fieldName) {
        if (getIn(errors, touched, fieldName) && (errors && touched)) {
            return {
                border: '1px solid red'
            }
        }
    }
    return (
        props.isnuminput==='true' ? (<>
                <label  className='input-form' htmlFor={label}>
                    {label}

                    <PatternFormat
                        {...field} {...rest} style={getStyles(form.errors[name], form.touched[name], field.name)}
                        displayType="input"
                        format="(###)-###-##-##"
                        mask="_"
                    />
                </label>
            {form.errors[name] && form.touched[name] && <div className='error'>{form.errors[name]}</div>}
        </>
    ) : ( <>
                <label  className='input-form' htmlFor={label}> {label}
                    <input {...field} {...rest}
                           style={getStyles(form.errors[name], form.touched[name], field.name)}
                    />
                </label>
                {form.errors[name] && form.touched[name] && <div className='error'>{form.errors[name]}</div>}
            </>)
    )
}

export function InputNumber (props){
    const {field, form, label, ...rest} = props;
    const {name} = field;
    function getStyles(errors, touched, fieldName) {
        if (getIn(errors, touched, fieldName) && (errors && touched)) {
            return {
                border: '1px solid red'
            }
        }
    }
    return (
        <>
            <label  className='input-form' htmlFor={label}> {label}
                {<PatternFormat
                    {...field} {...rest} style={getStyles(form.errors[name], form.touched[name], field.name)}
                    displayType="input"
                    format="+38 (###) ### ## ##"
                    mask="_"
                />}
            </label>
            {form.errors[name] && form.touched[name] && <div className='error'>{form.errors[name]}</div>}
        </>
    )
}