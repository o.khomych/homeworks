import DeleteIcon from "./DeleteIcon";
import renderer from 'react-test-renderer';
test("Delete icon snapshot",  () => {
    const tree = renderer.create('<DeleteIcon/>').toJSON();
    expect(tree).toMatchSnapshot();
})