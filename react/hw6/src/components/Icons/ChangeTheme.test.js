import ChangeTheme from "./ChangeTheme";
import renderer from 'react-test-renderer';
test("ChangeTheme snapshot ",  () => {
    const clickBtn = jest.fn()
    const tree = renderer.create('<ChangeTheme clickHandler={clickBtn}/>').toJSON();
    expect(tree).toMatchSnapshot();
})
