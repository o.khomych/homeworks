import Cart from "./Cart";
import renderer from 'react-test-renderer';
test("CartIcon",  () => {
    const tree = renderer.create('<Cart className="test"/>').toJSON();
    expect(tree).toMatchSnapshot();
})
