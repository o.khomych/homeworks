import FavoriteIcon from "./FavoriteIcon";
import renderer from 'react-test-renderer';
test("FavIcon snapshot isFav",  () => {
    const clickBtn = jest.fn()
    const tree = renderer.create('<FavoriteIcon isFav={true} clickHandler={clickBtn}/>').toJSON();
    expect(tree).toMatchSnapshot();
})
test("FavIcon snapshot notFav",  () => {
    const clickBtn = jest.fn()
    const tree = renderer.create('<FavoriteIcon isFav={false} clickHandler={clickBtn}/>').toJSON();
    expect(tree).toMatchSnapshot();
})
