import Modal from "./Modal";
import {fireEvent, render, screen} from "@testing-library/react"
import React from 'react'
import '@testing-library/jest-dom'
describe('Modal testing', () => {
    test('Component should rendering' ,() => {
        render(<Modal clickHandler={jest.fn()}
            closeButton={true}
                      header='test-header'
                      text='test-text'
                      action={<div>test</div>}
        />)
        expect(screen.getByRole('modal')).toBeInTheDocument()
        expect(screen.getByText('test-header')).toBeInTheDocument()
        expect(screen.getByText('test-text')).toBeInTheDocument()
        expect(screen.getByText('test')).toBeTruthy()

    })
    test('Modal shows a close button', ()=>{
        const handleClose = jest.fn()
        const view = render(<Modal clickHandler={handleClose}
                                   closeButton={true}
        />)
        const clsBtn = screen.getByTestId('cls-btn')
        expect(clsBtn).toBeInTheDocument()
        expect(clsBtn).toHaveClass('modal__content__close-btn')
        fireEvent.click(clsBtn)
        expect(handleClose).toHaveBeenCalledTimes(1)
    })
})