import React, {useEffect, useState} from 'react'
import './item.scss'
import propTypes from 'prop-types';
import Icon from "../Icons/FavoriteIcon";
import '../../scss/style.scss'
import {ThemeContext} from "../../context";
import {useContext} from "react";


export default function Item (props){

    const {theme} = useContext(ThemeContext)
    const [type, setType] = useState(false)
    useEffect(()=>{
        if (localStorage.getItem(`${props.article}`) !== null) {
            setType(true)
        } else{
            setType(false)
        }
    })
    function clickFav () {
        if (!type) {
            localStorage.setItem(`${props.article}`, true)
                setType(true)

        } else {
            localStorage.removeItem(`${props.article}`)
                setType(true)
        }
    }
        return (
            <li className={props.productItem} id={props.article}>
                <div className={theme.imageContainer}>
                    <img src={props.image} alt="" className={props.img}/>
                    <div className={theme.itemButton}>
                        <Icon isFav={type}
                              clickHandler={()=> {
                                  clickFav()
                                  props.addToFav()
                              }}
                                />
                    </div>
                </div>
                <div className={theme.itemDetails}>
                    <h3 className={theme.itemDetails__heading}>
                        {props.name}
                    </h3>
                    <p className={theme.itemDetailsPrice}>{props.price} UAH</p>
                    <span className={theme.itemDetailsColor} style={{backgroundColor: props.color}}></span>
                    {props.action}
                </div>
            </li>
    );
    }
// Item.propTypes = {
//     article: propTypes.number.isRequired,
//     name: propTypes.string.isRequired,
//     price: propTypes.number.isRequired,
//     color: propTypes.string.isRequired,
//     image: propTypes.string.isRequired,
//     action: propTypes.element.isRequired,
//     addToFav: propTypes.func,
// }