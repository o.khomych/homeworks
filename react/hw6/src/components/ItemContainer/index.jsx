import React from "react";
import Item from "../Item/index";
import '../Item/item.scss'
import Button from "../Button";
import propTypes from 'prop-types';
import {useEffect, useState} from 'react'
import {ThemeContext} from "../../context";
import {useContext} from "react";
export default function ItemsContainer (props){
    const listItem = props.items
    const {theme} = useContext(ThemeContext)
    const type = false
        return (<div className='items-container'>
            <ul className={props.itemList}>
                {listItem.map(({name, price, color, url, article}) => {
                    return <Item
                        name={name}
                        price={price}
                        color={color}
                        image={url}
                        key={article}
                        id={article}
                        article={article}
                        img={props.img}
                        productItem={props.productItem}
                        addToFav={() => props.addToFav({article})}
                        action={
                            <Button className={theme.addToCart} text={props.text}
                                    clickHandler={() => props.action({article})}
                            />
                        }
                    />
                })}

            </ul>
        </div>)
}
// ItemsContainer.propTypes = {
//     items: propTypes.array.isRequired,
//     action: propTypes.func.isRequired,
//     addToFav: propTypes.func.isRequired
// }
