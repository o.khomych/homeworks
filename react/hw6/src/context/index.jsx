import {createContext} from "react";
export const themes = {
    cardsTheme:{
        itemList: 'items-list',
        imageContainer: "image-container",
        itemButton: "item-button",
        itemDetails: "item-details",
        itemDetails__heading: "item-details__heading",
        itemDetailsPrice: "item-details__price",
        itemDetailsColor:"item-details__color",
        image: 'image',
        imageCart: 'image cart',
        productItem:'product-item',
        productItemCart:'product-item cart',
        addToCart: 'add-to-cart',
        iconFill: 'not-fav-icon'
    },
    tableTheme:{
        itemList: 'items-list table',
        imageContainer: "image-container table",
        itemButton: "item-button table",
        itemDetails: "item-details table",
        itemDetails__heading: "item-details__heading table",
        itemDetailsPrice: "item-details__price table",
        itemDetailsColor:"item-details__color table",
        image: 'image table',
        imageCart: 'image cart table',
        productItem:'product-item table',
        productItemCart:'product-item cart table',
        addToCart: 'add-to-cart table',
        iconFill: 'not-fav-icon table',
    },
}
export const ThemeContext = createContext(themes.cardsTheme);
