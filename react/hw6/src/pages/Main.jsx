import React, {useContext, useState} from 'react'
import Button from "../components/Button";
import '../scss/style.scss'
import Modal from "../components/Modal/Modal";
import ItemContainers from '../components/ItemContainer'
import Header from "../components/Header";
import CartIcon from "../components/Icons/Cart";
import '../components/Item/item.scss'
import {ThemeContext} from "../context";
export default function Main (props){

    const {theme} = useContext(ThemeContext)
        return ( <div className={'main-container'} >
            <ItemContainers
                items={props.items}
                action={props.openModal}
                addToFav={props.addToFav}
                text={<><CartIcon/><span>Add to cart</span></>}
                type={props.type}
                itemList={theme.itemList}
                productItem={theme.productItem}
                img={theme.image}
            />

            {props.isOpen && <Modal
                header="Shopping bag"
                closeButton={true}
                text='Do you want add this item to cart?'
                clickHandler={props.closeModal}
                action={
                    <div  className='modal__content__footer__btn-wrap'>
                        <Button className='modal__content__footer__btn'
                                text="Add"
                                clickHandler={props.addToOrder}></Button>
                        <Button className='modal__content__footer__btn'
                                clickHandler={props.closeModal}
                                text="Cancel"></Button>
                    </div>}
            >
            </Modal>}
        </div>
    )
}