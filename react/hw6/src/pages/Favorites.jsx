import Button from "../components/Button";
import DeleteIcon from "../components/Icons/DeleteIcon";
import ItemsContainer from "../components/ItemContainer";
import React, {useContext} from "react";
import CartIcon from "../components/Icons/Cart";
import Modal from "../components/Modal/Modal";
import {ThemeContext} from "../context";

export default function Favorites (props){

    const {theme} = useContext(ThemeContext)
    return (
        <div className='main-container'>
            <h1 className='fav-header'>Favorites</h1>
            {props.favoriteItem.length > 0 ? <ItemsContainer
                items={props.favoriteItem}
                action={props.openModal}
                text={<><CartIcon/>Add</>}
                btnClassName="add-to-cart"
                addToFav={props.addToFav}
                productItem={theme.productItem}
                itemList={theme.itemList}
                img={theme.image}
            /> : <div className='empty'>
                {/*<h1>Favorites</h1>*/}
                <h2>SAVE YOUR FAVORITE ITEMS</h2>
                <p>Want to save the items you love?</p>
                <p>Just click on the heart icon found on the product image and it will show up here.</p>
            </div>}
            {props.isOpen && <Modal
                header="Shopping bag"
                closeButton={true}
                text='Do you want add this item to cart?'
                clickHandler={props.closeModal}
                action={
                    <div  className='modal__content__footer__btn-wrap'>
                        <Button className='modal__content__footer__btn'
                            // clickHandler={this.addToCart}
                                text="Add"
                                clickHandler={props.addToOrder}></Button>
                        <Button className='modal__content__footer__btn'
                                clickHandler={props.closeModal}
                                text="Cancel"></Button>
                    </div>}
            >
            </Modal>}
        </div>
    )
}