import ItemsContainer from "../components/ItemContainer";
import DeleteIcon from "../components/Icons/DeleteIcon";
import Modal from "../components/Modal/Modal";
import Button from "../components/Button";
import React, {useContext} from "react";
import CheckoutForm from "../components/Form/Checkout";
import {ThemeContext, themes} from "../context";
import '../components/Item/item.scss'

export default function Cart (props){
    const {theme} = useContext(ThemeContext)
    return (
        props.orders.length > 0 ? (
            <div className='main-container '>
                <h1 className='cart-header'>Shopping bag</h1>
                <div className="cart-container">
                    <ItemsContainer
                        items={props.orders}
                        text={<><DeleteIcon/></>}
                        action={props.openModal}
                        addToFav={props.addToFav}
                        productItem={theme.productItemCart}
                        itemList={theme.itemList}
                        img={theme.imageCart}
                    />
                    <CheckoutForm onSubmit={props.onSubmit}/>
                </div>
                {props.isOpen && <Modal
                    header="Delete Item?"
                    closeButton={true}
                    text='Are you sure to delete this item?'
                    clickHandler={props.closeModal}
                    action={
                        <div  className='modal__content__footer__btn-wrap'>
                            <Button className='modal__content__footer__btn'
                                    text="Yes"
                                    clickHandler={props.deleteItem}></Button>
                            <Button className='modal__content__footer__btn'
                                    clickHandler={props.closeModal}
                                    text="No"></Button>
                        </div>}
                >

                </Modal>}
            </div>) : (<div className='empty'>
            <h1>Shopping bag</h1>
            <h2>YOUR SHOPPING BAG IS EMPTY!</h2>
        </div>)
    )

}