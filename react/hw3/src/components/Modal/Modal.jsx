import React, {Fragment} from 'react';
import './modal.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons'
import propTypes from 'prop-types';

export default function Modal (props){
        return (<div className='modal' onClick={props.clickHandler}>
            <div className='modal__content'
                 onClick={e=> e.stopPropagation()}
            >
                <div className='modal__content__header'>
                    <h3>{props.header}</h3>
                    <span className='modal__content__close-btn' onClick={props.clickHandler}>{props.closeButton && <FontAwesomeIcon icon={faXmark}/>}</span>
                </div>
                <div className='modal__content__body'>
                    <p>{props.text}</p>
                </div>

                <div className='modal__content__footer'>
                    {props.action}
                </div>
            </div>

        </div>)

}

// Modal.propTypes = {
//     clickHandler: propTypes.func.isRequired,
//     closeButton: propTypes.bool.isRequired,
//     header: propTypes.string.isRequired,
//     text: propTypes.string.isRequired,
//     action: propTypes.oneOfType([propTypes.arrayOf(propTypes.node), propTypes.node]).isRequired
// }
