import React from 'react'
import './button.scss'
import propTypes from 'prop-types'

export default function Button (props){
        return <button onClick={props.clickHandler}
                       style={{backgroundColor: props.backgroundColor}}
                        className={props.className}>
            {props.text}
        </button>
}

Button.propTypes = {
    clickHandler: propTypes.func.isRequired,
    text: propTypes.oneOfType([propTypes.string, propTypes.node, propTypes.element]).isRequired,
    backgroundColor: propTypes.string,
    className: propTypes.string.isRequired,
}
Button.defaultProps ={
    backgroundColor: 'none',
}