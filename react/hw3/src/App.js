import React, {useEffect, useState} from 'react'
import './scss/style.scss'
import Main from "./pages/Main";
import {Routes, Route, Navigate} from "react-router-dom";
import Cart from "./pages/Cart"
import Favorites from "./pages/Favorites"
import Header from "./components/Header";

export default function App (){
    const [items, setItems] = useState([])
    const [isOpen, setIsOpen] = useState(false)
    const [addOrder, setAddOrder] = useState(null)
    const [orders, setOrders] = useState( JSON.parse(localStorage.getItem('cart')) ||[])
    const [cartAmount, setCartAmount] = useState(+localStorage.getItem('cartAmount'))
    const [addFav, setAddFav] = useState(null)
    const [favoriteItem, setFavoriteItem] = useState(JSON.parse(localStorage.getItem('favorites')) || [])
    const [favAmount, setFavAmount] = useState(+localStorage.getItem('favAmount'))
    useEffect(()=>{
        fetch('/items.json')
            .then(res => res.json())
            .then(result =>{
                setItems(result)
            })
    }, [items])

    useEffect(()=>{
        localStorage.setItem('cartAmount', cartAmount)
    }, [cartAmount])

    useEffect(()=>{
        localStorage.setItem('cart', JSON.stringify(orders))
    }, [orders])
    useEffect(()=>{
        localStorage.setItem('favAmount', favAmount)
    }, [favAmount])

    useEffect(()=>{
        localStorage.setItem('favorites', JSON.stringify(favoriteItem))
    }, [favoriteItem])

    function addToOrder () {
        let isInArray = false
        orders.forEach(el=>{
            if(el.article === addOrder) {
                isInArray = true
            }
        })
        if (!isInArray) {
            const data = items.find(item => item.article === addOrder)
            setOrders([...orders, data])
            setCartAmount(cartAmount + 1)
        }
        setIsOpen(false)
    }

    function openModal (el){
        setIsOpen(true)
        setAddOrder(el.article)
    }
    function closeModal (){
        setIsOpen(false)
    }
    function deleteItem(){
        const data = orders.filter(item => item.article !== addOrder)
        setOrders(data)
        setCartAmount(cartAmount - 1)
        setIsOpen(false)
    }
    function addToFav(elem){
        let isInArray = false
        favoriteItem.forEach(el=>{
            if(el.article === elem.article) {
                isInArray = true
            }
        })
        if (!isInArray) {
            const addData = items.find(item => item.article === elem.article)
            setFavoriteItem([...favoriteItem, addData])
            setFavAmount(favAmount + 1)
        } else{
            const delData = favoriteItem.filter(item => item.article !== elem.article)
            setFavoriteItem(delData)
            setFavAmount(favAmount - 1)
        }
    }
    return (
        <Routes>
            <Route path='/' element={<Header favoriteCount={favAmount} cartCount={cartAmount}/>}>
                <Route index element={<Navigate to={'/main'}/>}/>
                <Route path='/main'
                       element={<Main
                           items={items}
                           addToOrder={addToOrder}
                           openModal={openModal}
                           closeModal={closeModal}
                           isOpen = {isOpen}
                           addToFav={addToFav}
                       />}/>
                <Route path='/cart' element={<Cart
                    orders={orders}
                    isOpen = {isOpen}
                    openModal={openModal}
                    closeModal={closeModal}
                    deleteItem={deleteItem}
                    addToFav={addToFav}
                />} />
                <Route path='/favorites' element={<Favorites
                    favoriteItem={favoriteItem}
                    addToOrder={addToOrder}
                    openModal={openModal}
                    closeModal={closeModal}
                    isOpen = {isOpen}
                    addToFav={addToFav}
                />}/>
            </Route>
        </Routes>
    )

}
