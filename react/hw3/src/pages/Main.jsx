import React, {useState} from 'react'
import Button from "../components/Button";
import '../scss/style.scss'
import Modal from "../components/Modal/Modal";
import ItemContainers from '../components/ItemContainer'
import Header from "../components/Header";
import CartIcon from "../components/Icons/Cart";

export default function Main (props){
    // state = {
    //     isOpen: false,
    //     favoriteList: localStorage.getItem('favoriteList'),
    //     favorite: localStorage.getItem('favoriteAmount'),
    //     cartList:  localStorage.getItem('cart'),
    //     cart: localStorage.getItem('cartAmount'),
    //     id:null,
    //     isFav: false
    // }
    // componentDidMount() {
    //
    //     if (localStorage.getItem('cart') === null) {
    //         this.setState(()=>{
    //             return {cart: 0,
    //                 cartList:[null]}
    //         })
    //         } else{
    //         this.setState(()=>{
    //             return {
    //                 cart: (localStorage.getItem('cartAmount')),
    //                 cartList: (localStorage.getItem('cart')).split(',')}
    //         })
    //     }
    //     if (localStorage.getItem('favoriteList') === null) {
    //         this.setState(()=>{
    //             return {favorite: 0,
    //                     favoriteList: []}
    //         })
    //     } else{
    //         this.setState(()=>{
    //             return {
    //                 favorite: (localStorage.getItem('favoriteAmount')),
    //                 favoriteList: (localStorage.getItem('favoriteList')).split(',')}
    //         })
    //     }
    // }
    // componentDidUpdate(prevProps, prevState) {
    //     if (this.state.cartList !== prevState.cartList){
    //         localStorage.setItem('cart', this.state.cartList)
    //         localStorage.setItem('cartAmount', this.state.cart)
    //     }
    //     if (this.state.favoriteList !== prevState.favoriteList){
    //         localStorage.setItem('favoriteAmount', this.state.favorite)
    //         localStorage.setItem('favoriteList', this.state.favoriteList)
    //     }
    // }

    // addToCart = () => {
    //     if ( localStorage.getItem('cart').includes(String(this.state.id))) {
    //         this.setState(()=>{
    //             return {
    //                 isOpen: false,
    //             }
    //         })
    //     } else{
    //         const data = this.state.items.find(item => item.article === this.state.id)
    //         localStorage.setItem('cart', this.state.cartList)
    //         localStorage.setItem('cartAmount', this.state.cart)
    //         this.setState(()=>{
    //             return {
    //                 cartList: [...this.state.cartList, [data.article]],
    //                 isOpen: false,
    //                 cart: +this.state.cart + 1
    //             }
    //         })
    //     }
    // }

    // clickFav = (id)=>{
    //     if (this.state.favoriteList.includes((id.article)) || this.state.favoriteList.includes(String(id.article))) {
    //         let data = this.state.favoriteList.filter(item => Number(item) !== Number(id.article))
    //             this.setState(() => {
    //                 if (data[0]==='' && data.length===1){
    //                     return {
    //                         favoriteList: data.shift(),
    //                         favorite: 0
    //                     }
    //                 } else{
    //                     return {
    //                         favoriteList: data,
    //                         favorite: data.length
    //                     }
    //                 }
    //
    //             })
    //             localStorage.setItem('favoriteList', data)
    //             localStorage.setItem('favoriteAmount', this.state.favorite)
    //
    //
    //     } else {
    //         const dat = this.state.items.find(item => item.article === id.article)
    //         localStorage.setItem('favoriteList', this.state.favoriteList)
    //         localStorage.setItem('favoriteAmount', this.state.favorite)
    //         this.setState(() => {
    //                 return {
    //                     favoriteList: [...this.state.favoriteList, dat.article],
    //                     favorite: +this.state.favorite + 1
    //                 }
    //         })
    //     }
    //     console.log(this.state.favoriteList)
    // }
        return ( <div className={'main-container'} >
            {/*<Header favoriteCount={this.state.favorite} cartCount={this.state.cart}/>*/}
            <ItemContainers
                items={props.items}
                action={props.openModal}
                addToFav={props.addToFav}
                text={<><CartIcon/>Add to cart</>}
                btnClassName="add-to-cart"
                type={props.type}
            />

            {props.isOpen && <Modal
                header="Shopping bag"
                closeButton={true}
                text='Do you want add this item to cart?'
                clickHandler={props.closeModal}
                action={
                    <div  className='modal__content__footer__btn-wrap'>
                        <Button className='modal__content__footer__btn'
                                // clickHandler={this.addToCart}
                                text="Add"
                                clickHandler={props.addToOrder}></Button>
                        <Button className='modal__content__footer__btn'
                                clickHandler={props.closeModal}
                                text="Cancel"></Button>
                    </div>}
            >
            </Modal>}
        </div>
    )
}
