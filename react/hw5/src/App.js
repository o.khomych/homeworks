import React, {useEffect, useState} from 'react'
import './scss/style.scss'
import Main from "./pages/Main";
import {Routes, Route, Navigate} from "react-router-dom";
import Cart from "./pages/Cart"
import Favorites from "./pages/Favorites"
import Header from "./components/Header";
import { useDispatch, useSelector } from "react-redux";
import {loadItems, showModal, hideModal} from "./store/action";


export default function App (){
    const [addOrder, setAddOrder] = useState(null)
    const [orders, setOrders] = useState( JSON.parse(localStorage.getItem('cart')) ||[])
    const [cartAmount, setCartAmount] = useState(+localStorage.getItem('cartAmount'))
    const [favoriteItem, setFavoriteItem] = useState(JSON.parse(localStorage.getItem('favorites')) || [])
    const [favAmount, setFavAmount] = useState(+localStorage.getItem('favAmount'))
    const dispatch = useDispatch()

    const {items, modal} = useSelector(state => ({
        items: state.items,
        modal: state.modal,
    }))

    useEffect(()=>{
        dispatch(loadItems())
    }, [])

    useEffect(()=>{
        localStorage.setItem('cartAmount', cartAmount)
    }, [cartAmount])

    useEffect(()=>{
        localStorage.setItem('cart', JSON.stringify(orders))
    }, [orders])
    useEffect(()=>{
        localStorage.setItem('favAmount', favAmount)
    }, [favAmount])

    useEffect(()=>{
        localStorage.setItem('favorites', JSON.stringify(favoriteItem))
    }, [favoriteItem])

    function addToOrder () {
        let isInArray = false
        orders.forEach(el=>{
            if(el.article === addOrder) {
                isInArray = true
            }
        })
        if (!isInArray) {
            const data = items.find(item => item.article === addOrder)
            setOrders([...orders, data])
            setCartAmount(cartAmount + 1)
        }
        dispatch(hideModal())
    }

    function actionShowModal (el){
        dispatch(showModal())
        setAddOrder(el.article)
    }
    function actionHideModal (){
        dispatch(hideModal())
    }
    function deleteItem(){
        const data = orders.filter(item => item.article !== addOrder)
        setOrders(data)
        setCartAmount(cartAmount - 1)
        // setTotal(orders.reduce((a, b) => a + b.price, 0))
        dispatch(hideModal())
    }
    function addToFav(elem){
        let isInArray = false
        favoriteItem.forEach(el=>{
            if(el.article === elem.article) {
                isInArray = true
            }
        })
        if (!isInArray) {
            const addData = items.find(item => item.article === elem.article)
            setFavoriteItem([...favoriteItem, addData])
            setFavAmount(favAmount + 1)
        } else{
            const delData = favoriteItem.filter(item => item.article !== elem.article)
            setFavoriteItem(delData)
            setFavAmount(favAmount - 1)
        }
    }

    const sendForm = (values, action) => {
        const clientInfo = `Client information:
        First name: ${values.firstName}
        Last name: ${values.lastName}
        Age: ${values.age}
        Billing address: ${values.address}
        Phone number: ${values.phoneNumber} `
        console.log(clientInfo)
        orders.forEach(el =>{
            console.log(el)
        })
        setOrders([])
        setCartAmount(0)
        console.log(values.phoneNumber.length)
    }
    return (
        <Routes>
            <Route path='/' element={<Header favoriteCount={favAmount} cartCount={cartAmount}/>}>
                <Route index element={<Navigate to={'/main'}/>}/>
                <Route path='/main'
                       element={<Main
                           items={items}
                           addToOrder={addToOrder}
                           openModal={actionShowModal}
                           closeModal={actionHideModal}
                           isOpen = {modal}
                           addToFav={addToFav}
                       />}/>
                <Route path='/cart' element={<Cart
                    orders={orders}
                    isOpen = {modal}
                    openModal={actionShowModal}
                    closeModal={actionHideModal}
                    deleteItem={deleteItem}
                    addToFav={addToFav}
                    onSubmit={sendForm}
                />} />
                <Route path='/favorites' element={<Favorites
                    favoriteItem={favoriteItem}
                    addToOrder={addToOrder}
                    openModal={actionShowModal}
                    closeModal={actionHideModal}
                    isOpen = {modal}
                    addToFav={addToFav}
                />}/>
            </Route>
        </Routes>
    )

}
