import React, {useState} from 'react'
import Button from "../components/Button";
import '../scss/style.scss'
import Modal from "../components/Modal/Modal";
import ItemContainers from '../components/ItemContainer'
import Header from "../components/Header";
import CartIcon from "../components/Icons/Cart";
import '../components/Item/item.scss'
export default function Main (props){
        return ( <div className={'main-container'} >
            <ItemContainers
                items={props.items}
                action={props.openModal}
                addToFav={props.addToFav}
                text={<><CartIcon/>Add to cart</>}
                btnClassName="add-to-cart"
                type={props.type}
                img='image'
                productItem="product-item"
            />

            {props.isOpen && <Modal
                header="Shopping bag"
                closeButton={true}
                text='Do you want add this item to cart?'
                clickHandler={props.closeModal}
                action={
                    <div  className='modal__content__footer__btn-wrap'>
                        <Button className='modal__content__footer__btn'
                                text="Add"
                                clickHandler={props.addToOrder}></Button>
                        <Button className='modal__content__footer__btn'
                                clickHandler={props.closeModal}
                                text="Cancel"></Button>
                    </div>}
            >
            </Modal>}
        </div>
    )
}