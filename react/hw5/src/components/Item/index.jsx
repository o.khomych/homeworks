import React, {useEffect, useState} from 'react'
import './item.scss'
import propTypes from 'prop-types';
import Icon from "../Icons/FavoriteIcon";
import '../../scss/style.scss'
export default function Item (props){
    const [type, setType] = useState(false)
    useEffect(()=>{
        if (localStorage.getItem(`${props.article}`) !== null) {
            setType(true)
        } else{
            setType(false)
        }
    })
    function clickFav () {
        if (!type) {
            localStorage.setItem(`${props.article}`, true)
                setType(true)

        } else {
            localStorage.removeItem(`${props.article}`)
                setType(true)
        }
    }
        return (
            <li className={props.productItem} id={props.article}>
                <div className="image-container">
                    <img src={props.image} alt="" className={props.img}/>
                        <div className="item-button">
                            <Icon isFav={type}
                                  clickHandler={()=> {
                                      clickFav()
                                      props.addToFav()
                                  }}
                                    />
                        </div>
                </div>
                <div className="item-details">
                    <h3 className="item-details__heading">
                        {props.name}
                    </h3>
                    <p className="item-details__price">{props.price} UAH</p>
                    <span className="item-details__color" style={{backgroundColor: props.color}}></span>
                    {props.action}
                </div>
            </li>
    );
    }

// Item.propTypes = {
//     article: propTypes.number.isRequired,
//     name: propTypes.string.isRequired,
//     price: propTypes.number.isRequired,
//     color: propTypes.string.isRequired,
//     image: propTypes.string.isRequired,
//     action: propTypes.element.isRequired,
//     addToFav: propTypes.func,
// }