import React from 'react'
import Input from './index'
import {Field, Form, Formik} from 'formik';
import Button from "../Button";
import * as Yup from 'yup'

const CheckoutForm = (props) => {

    return (
        <Formik  initialValues={{
            firstName: '',
            lastName: '',
            age: '',
            address: '',
            phoneNumber: ''
        }}
            onSubmit={props.onSubmit}
            validationSchema={Yup.object({
            firstName: Yup.string().required('Please enter your first name')
                .matches(/^([^0-9.!#№()":,;@$%&'*+\/=?^_`{|}~-]*)$/gm, "Please enter correct name"),
            lastName: Yup.string().required('Please enter your last name')
                .matches(/^([^0-9.!#№()":,;@$%&'*+\/=?^_`{|}~-]*)$/gm, "Please enter correct last name"),
            age: Yup.number()
                .moreThan(18, 'You have to be at least 18 years old')
                .lessThan(99, 'Please enter correct age')
                .positive('Please enter correct age')
                .integer('Please enter correct age')
                .required('Please enter your age'),
            address: Yup.string()
                .min(10, 'Please enter full address')
                .required('Please enter your billing address'),
            phoneNumber: Yup.string().length(15, 'Enter phone number').required('Please enter your phone number'),
            })}
             >
            <Form className='checkout-form' noValidate>
                <h3>My information</h3>
                <Field
                    component={Input}
                    label="First name"
                    type="text"
                    placeholder="Enter your first name"
                    name="firstName"
                />
                <Field
                    component={Input}
                    label="Last name"
                    type="text"
                    placeholder="Enter your last name"
                    name="lastName"
                />
                <Field
                    component={Input}
                    label="Age"
                    type="number"
                    placeholder="How old are you?"
                    name="age"
                />
                <Field
                    component={Input}
                    label="Billing address"
                    type="text"
                    placeholder="Enter your billing address"
                    name="address"
                />
                <Field
                    component={Input}
                    label="Phone number"
                    type='text'
                    placeholder="(___)-___-__-__"
                    name="phoneNumber"
                    isnuminput= 'true'
                />
                <div className='checkout-form__btn-wrap'>
                    <Button className='checkout-form__btn' text="Checkout" type='submit'/>
                </div>
            </Form>
        </Formik>
    )
}

export default CheckoutForm