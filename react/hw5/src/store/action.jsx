export const ITEMS = 'ITEMS'
export const ShowModalAction = 'ShowModalAction'
export const HideModalAction = 'HideModalAction'
export const ORDERS = 'ORDERS'

export const loadItems = () => {
    return async (dispatch) => {
        const res = await fetch('../../items.json');
        dispatch({ type: ITEMS, payload: await res.json() });
    }
}

export const showModal = () => {
    return (dispatch) => {
        dispatch({ type: ShowModalAction, payload: true})
    }
}
export const hideModal = () => {
    return (dispatch) => {
        dispatch({ type: HideModalAction, payload: false})
    }
}
