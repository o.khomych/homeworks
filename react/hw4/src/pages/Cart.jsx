import ItemsContainer from "../components/ItemContainer";
import DeleteIcon from "../components/Icons/DeleteIcon";
import Modal from "../components/Modal/Modal";
import Button from "../components/Button";
import React from "react";

export default function Cart (props){
    console.log(props.orders)
    return (
        props.orders.length > 0 ? (
            <div className='main-container'>
                <h1 className='cart-header'>Shopping bag</h1>
                <ItemsContainer
                    items={props.orders}
                    text={<><DeleteIcon/></>}
                    btnClassName='delete-icon'
                    action={props.openModal}
                    addToFav={props.addToFav}
                />
                {props.isOpen && <Modal
                    header="Delete Item7"
                    closeButton={true}
                    text='Are you sure to delete this item?'
                    clickHandler={props.closeModal}
                    action={
                        <div  className='modal__content__footer__btn-wrap'>
                            <Button className='modal__content__footer__btn'
                                    text="Yes"
                                    clickHandler={props.deleteItem}></Button>
                            <Button className='modal__content__footer__btn'
                                    clickHandler={props.closeModal}
                                    text="No"></Button>
                        </div>}
                >
                </Modal>}
            </div>) : (<div className='empty'>
            <h1>Shopping bag</h1>
            <h2>YOUR SHOPPING BAG IS EMPTY!</h2>
        </div>)
    )

}