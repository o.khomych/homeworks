import {ITEMS, showModal, hideModal, ShowModalAction, HideModalAction} from './action'
const initialState = {
    items: [],
    modal: false,
}

export const rootReducer = (state = initialState, action) =>{
    switch (action.type) {
        case ITEMS:
            return { ...state, items: action.payload }
        case ShowModalAction:
            return {...state, modal: action.payload}
        case HideModalAction:
            return {...state, modal: action.payload}
        default:
            return { ...state }
    }
}