import React from 'react'
import FavoriteIcon from "../Icons/FavoriteIcon";
import Cart from '../Icons/Cart'
import './header.scss'
import '../../scss/style.scss'
import propTypes from 'prop-types';
import {Link, Outlet} from "react-router-dom";

export default function Header (props){
    return (
        <>
            <header className='header'>
                <Link to='/' className='header__logo'>
                    <img src="/images/UK.png" alt="logo"/>
                </Link>
                <ul className='header__icon-container'>
                    <Link to='favorites' className='header__icon-container__favorite-icon'>
                        <FavoriteIcon isFav={false}/>
                        <span
                            className='header__icon-container__favorite-icon__text'>Favorites ({props.favoriteCount})</span>
                    </Link>
                    <Link to='cart' className='header__icon-container__cart-icon'>
                        <Cart className='header__icon-container__cart-icon__icon'/>
                        <span
                            className='header__icon-container__cart-icon__text'>Shopping bag ({props.cartCount})</span>
                    </Link>
                </ul>
            </header>
            <Outlet/>
        </>
    )
}

Header.propTypes = {
    // favoriteCount: propTypes.number,
    // cartCount: propTypes.string
}
Header.defaultProps ={
    favoriteCount: 0,
    cartCount: 0
}