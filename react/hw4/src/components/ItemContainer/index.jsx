import React from "react";
import Item from "../Item/index";
import '../Item/item.scss'
import Button from "../Button";
import propTypes from 'prop-types';
import {useEffect, useState} from 'react'

export default function ItemsContainer (props){
        const listItem = props.items
    const type = false
        return (<div className='items-container'>
            <ul className='items-list'>
                {listItem.map(({name, price, color, url, article}) => {
                    return <Item
                        name={name}
                        price={price}
                        color={color}
                        image={url}
                        key={article}
                        id={article}
                        article={article}
                        addToFav={() => props.addToFav({article})}
                        action={
                            <Button className={props.btnClassName} text={props.text}
                                    clickHandler={() => props.action({article})}
                            />
                        }
                    />
                })}

            </ul>
        </div>)
}
// ItemsContainer.propTypes = {
//     items: propTypes.array.isRequired,
//     action: propTypes.func.isRequired,
//     addToFav: propTypes.func.isRequired
// }