import React from 'react'
import Button from "../components/Button/index";
import '../scss/style.scss'
import Modal from "../components/Modal/Modal";
import ItemContainers from '../components/ItemContainer/index'
import Header from "../components/Header";

export default class Main extends React.Component{
    state = {
        isOpen: false,
        items: [],
        favoriteList: localStorage.getItem('favoriteList'),
        favorite: localStorage.getItem('favoriteAmount'),
        cartList:  localStorage.getItem('cart'),
        cart: localStorage.getItem('cartAmount'),
        id:null,
        isFav: false
    }
    componentDidMount() {
        fetch('/items.json')
            .then(res => res.json())
            .then(result =>{
                this.setState({
                    items: result
                })
            })
            this.setState(()=>{
                return {
                    favorite: (localStorage.getItem('favoriteAmount') || 0),
                    favoriteList: (JSON.parse(localStorage.getItem('favoriteList')) || []),
                    cart: (localStorage.getItem('cartAmount') || 0),
                    cartList: (JSON.parse(localStorage.getItem('cart')) || [])
                }
            })
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.state.cartList !== prevState.cartList){
            localStorage.setItem('cart', JSON.stringify(this.state.cartList))
            localStorage.setItem('cartAmount', this.state.cart)
        }
        if (this.state.favoriteList !== prevState.favoriteList){
            localStorage.setItem('favoriteAmount', this.state.favorite)
            localStorage.setItem('favoriteList', JSON.stringify(this.state.favoriteList))
        }
    }
    openModal = (article) =>{
        this.setState(()=>{
            return {isOpen: true,
            id:article.article}
        })
    }
    closeModal =()=>{
        this.setState(()=>{
            return {isOpen: false}
        })
    }
    addToCart = () => {
        if ( localStorage.getItem('cart').includes(String(this.state.id))) {
            this.setState(()=>{
                return {
                    isOpen: false,
                }
            })
        } else{
            const data = this.state.items.find(item => item.article === this.state.id)
            localStorage.setItem('cart', JSON.stringify(this.state.cartList))
            localStorage.setItem('cartAmount', this.state.cart)
            this.setState(()=>{
                return {
                    cartList: [...this.state.cartList, data.article],
                    isOpen: false,
                    cart: +this.state.cart + 1
                }
            })
        }
    }

    clickFav = (id)=>{
        if (this.state.favoriteList.includes((id.article)) || this.state.favoriteList.includes(String(id.article))) {
            let data = this.state.favoriteList.filter(item => Number(item) !== Number(id.article))
                this.setState(() => {
                    if (data[0]==='' && data.length===1){
                        return {
                            favoriteList: data.shift(),
                            favorite: 0
                        }
                    } else{
                        return {
                            favoriteList: data,
                            favorite: data.length
                        }
                    }

                })
                localStorage.setItem('favoriteList', JSON.stringify(data))
                localStorage.setItem('favoriteAmount', this.state.favorite)


        } else {
            const dat = this.state.items.find(item => item.article === id.article)
            localStorage.setItem('favoriteList', JSON.stringify(this.state.favoriteList))
            localStorage.setItem('favoriteAmount', this.state.favorite)
            this.setState(() => {
                    return {
                        favoriteList: [...this.state.favoriteList, dat.article],
                        favorite: +this.state.favorite + 1
                    }
            })
        }
        console.log(this.state.favoriteList)
    }
    render(){
        return <div className={'main'} >
            <Header favoriteCount={this.state.favorite} cartCount={this.state.cart}/>
            <ItemContainers
                items={this.state.items}
                clickHandler={this.openModal}
                addToCart={this.openModal}
                addToFav={this.clickFav}
            />

            {this.state.isOpen && <Modal
                header="Shopping bag"
                closeButton={true}
                text='Do you want add this item to cart?'
                clickHandler={this.closeModal}
                action={
                    <div  className='modal__content__footer__btn-wrap'>
                        <Button className='modal__content__footer__btn' clickHandler={this.addToCart}
                                text="Add"></Button>
                        <Button className='modal__content__footer__btn' clickHandler={this.closeModal} text="Cancel"></Button>
                    </div>}
            >
            </Modal>}
        </div>
    }
}
