import React from 'react'
import FavoriteIcon from "../Icons/FavoriteIcon";
import Cart from '../Icons/Cart'
import './header.scss'
import propTypes from 'prop-types';

export default class Header extends React.Component {

    render(){
        return (
            <header className='header'>
                <li className='header__logo'>
                    <img src="/images/UK.png" alt="logo"/>
                </li>
                <ul className='header__icon-container'>

                    <li className='header__icon-container__favorite-icon'>
                        <FavoriteIcon isFav={false} />
                        <span className='header__icon-container__favorite-icon__text' >Favorites ({this.props.favoriteCount})</span>
                    </li>
                    <li className='header__icon-container__cart-icon'>
                        <Cart className='header__icon-container__cart-icon__icon'/>
                        <span className='header__icon-container__cart-icon__text'>Shopping bag ({this.props.cartCount})</span>
                    </li>
                </ul>
            </header>
        )
    }
}

Header.propTypes = {
    // favoriteCount: propTypes.number,
    // cartCount: propTypes.string
}
Header.defaultProps ={
    favoriteCount: 0,
    cartCount: 0
}