import React from 'react'
import './item.scss'
import propTypes from 'prop-types';
import Icon from "../Icons/FavoriteIcon";

export default class Item extends React.Component{
    state = {
        id: this.props.article,
        type: false
    }
    componentDidMount() {
        if (localStorage.getItem(`${this.props.article}`) !== null) {
            this.setState(() => {
                return {type: true}
            })
        } else{
            this.setState(() => {
                return {type: false}
            })
        }
    }

    clickFav = (e) =>{
        if (!this.state.type) {
            localStorage.setItem(`${this.props.article}`, true)
            this.setState(()=>{
                return {type: true}
            })
        } else {
            localStorage.removeItem(`${this.props.article}`)
            this.setState(()=>{
                return {type: false}
            })
        }
    }
    render() {
        return (
            <li className="product-item" id={this.props.article}>
                <div className="image-container">
                    <img src={this.props.image} alt="" className="image"/>
                        <div className="item-button">
                            <Icon isFav={this.state.type}
                                  clickHandler={()=> {
                                      this.clickFav()
                                      this.props.addToFav()
                                  }}
                                    />
                        </div>
                </div>
                <div className="item-details">
                    <h3 className="item-details__heading">
                        {this.props.name}
                    </h3>
                    <p className="item-details__price">{this.props.price} UAH</p>
                    <span className="item-details__color" style={{backgroundColor: this.props.color}}></span>
                    {this.props.actionAddToCArt}
                </div>
            </li>
    );
    }}

Item.propTypes = {
    article: propTypes.number.isRequired,
    name: propTypes.string.isRequired,
    price: propTypes.number.isRequired,
    color: propTypes.string.isRequired,
    image: propTypes.string.isRequired,
    actionAddToCArt: propTypes.element.isRequired,
    addToFav: propTypes.func,
}