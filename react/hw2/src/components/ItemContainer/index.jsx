import React from "react";
import Item from "../Item/index";
import '../Item/item.scss'
import propTypes from 'prop-types';
import Button from "../Button";
import Cart from "../Icons/Cart";

export default class ItemsContainer extends React.Component{
    render() {
        const listItem = this.props.items
        const type = false
        return <div className='items-container'>
            <ul className='items-list'>
                {listItem.map(({name, price, color, url, article})=>{
                    return <Item
                        name={name}
                        price={price}
                        color={color}
                        image={url}
                        key={article}
                        id={article}
                        article={article}
                        addToFav={()=>this.props.addToFav({article})}
                        actionAddToCArt={
                            <Button className="add-to-cart" text={<><Cart/>Add to cart</>} clickHandler={()=>this.props.addToCart({article})}/>
                        }
                    />
                })}

            </ul>
        </div>

    }
}
ItemsContainer.propTypes = {
    items: propTypes.array.isRequired,
    addToCart: propTypes.func.isRequired,
    addToFav: propTypes.func.isRequired
}