import React from 'react'
import './button.scss'
import propTypes from 'prop-types'

export default class Button extends React.Component{
    render(){
        return <button onClick={this.props.clickHandler}
                       style={{backgroundColor: this.props.backgroundColor}}
                        className={this.props.className}>
            {this.props.text}
        </button>
    }
}

Button.propTypes = {
    clickHandler: propTypes.func.isRequired,
    text: propTypes.oneOfType([propTypes.string, propTypes.node, propTypes.element]).isRequired,
    backgroundColor: propTypes.string,
    className: propTypes.string.isRequired,
}
Button.defaultProps ={
    backgroundColor: 'none',
}