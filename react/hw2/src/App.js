import React from 'react'
import './scss/style.scss'
import Main from "./pages/Main";


export default class App extends React.Component{
    render(){
        return <div>
            <Main/>
        </div>
    }
}
