// Our Services Tabs
document.querySelector('.our-services-tabs-list').addEventListener('click', function (e){
    let target = e.target.closest('.our-services-tab')
    if (target){
        document.querySelector('.our-services-tab.active').classList.remove('active')
        target.classList.add('active');
        document.querySelectorAll('.our-services-tabs-text').forEach(elem => {
            elem.classList.remove('active')
            if(elem.dataset.name === target.dataset.name){
                elem.classList.add('active')
            }
        })
    }
})

// Our Amazing Work

let activeSecondBl = false;
let activeThirdBl = false;
document.querySelector('#work-btn').addEventListener('click', function (){
    if (!activeSecondBl && !activeThirdBl){
        document.querySelector('.loading').style.display = 'flex'
        document.querySelector('#work-btn').style.display = 'none'
        activeSecondBl = true;
        setTimeout(addImg, 2000, '.second-block');
    } else if(activeSecondBl && !activeThirdBl){
        document.querySelector('.loading').style.display = 'flex'
        document.querySelector('#work-btn').style.display = 'none'
        activeThirdBl = true
        setTimeout(addImg, 2000, '.third-block')
    }
    function addImg (block){
        document.querySelectorAll(block).forEach(element => {
            if (document.querySelector('.work-tab.active').dataset.name === element.dataset.name) element.style.display = 'block'
            else if (document.querySelector('.work-tab.active').dataset.name === 'all') element.style.display = 'block'
        })
        if(activeThirdBl){
            document.querySelector('.loading').style.display = 'none'
            document.querySelector('#work-btn').style.display = 'none'
        }else{
            document.querySelector('.loading').style.display = 'none'
            document.querySelector('#work-btn').style.display = 'flex'
        }
    }
})


document.querySelector('.work-tabs').addEventListener('click', function (e){
    let target = e.target.closest('.work-tab')
    document.querySelector('.work-tab.active').classList.remove('active')
    target.classList.add('active')
    if (!activeSecondBl && !activeThirdBl){
        select('.frst-block')
        notSelect ('.second-block')
        notSelect ('.third-block')
    }
    if (activeSecondBl && !activeThirdBl){
        select('.frst-block')
        select('.second-block')
        notSelect ('.third-block')
    }
    if (activeSecondBl && activeThirdBl){
        select('.frst-block')
        select('.second-block')
        select('.third-block')
    }
     

    function select (selector){
        document.querySelectorAll(selector).forEach(e => {
            if (target.dataset.name === 'all'){
                e.style.display = 'block'
            } else if (target.dataset.name !== e.dataset.name){
                e.style.display = 'none'
            } else{
                e.style.display = 'block'
            }
        })}
    function notSelect (selector){
        document.querySelectorAll(selector).forEach(e => e.style.display = 'none')
    }
})


// Clients feedback
  
  $(document).ready(function(){
    $('.clients-block-photo').slick({
      arrows: true,
      dots: false,
      slidesToScroll:1,
      infinite: true,
      initialSlide:2,
      centerMode: true, 
      variableWidth: true,
      asNavFor: '.client-feedback-slider',
        focusOnSelect: true,
    
    })
    $('.client-feedback-slider').slick({
      arrows: false,
      fade: true,
      asNavFor: '.clients-block-photo',
      initialSlide:2,
      
      adaptiveHeight: true,
    })
  })

// Gllary-Masonry
let $grid = $('.gallery-photo').imagesLoaded(
    function(){
        $grid.masonry({
            columnWidth: 181,
            itemSelector: '.gallery-item',
            gutter: 14
          });
    })
  
let $grids = $('.grids').imagesLoaded(
    function(){
        $grids.masonry({
            columnWidth: 124,
            itemSelector: '.gallery-items',
            horizontalOrder: true,
            gutter: 3
          });
    })

  let imagesToAdd = [
    "vanglo-house-1%202.png",
    "80493541_1281644acb_o%201.png",
    "vanglo-house-6%201.png",
    "p1_8%201.png",
    "7328272788_c5048326de_o%201.png",
    "ringve-museum-1%201.png",
    "vanglo-house-1%202.png",
]


let arrImg = []
imagesToAdd.forEach(e=>{
    let div = document.createElement('div');
    let img = document.createElement('img');
    let srcImg = './css/img/gallery/' + e
    img.src = srcImg
    div.classList.add('gallery-item');
    let divHover = document.createElement('div');
    let divHoverBl1 = document.createElement('div');
    let divHoverBl2 = document.createElement('div');
    divHover.append(divHoverBl1);
    divHover.append(divHoverBl2);
    divHover.classList.add('gallery-img-hover')
    div.append(img);
    div.append(divHover)
    arrImg.push(div)
})

  $('#gallery-btn').on( 'click', function() {
    let $elems = $(arrImg);

    function addImg(){
        $grid.append( $elems ).masonry( 'appended', $elems );
        $('.loading-section-two').css("display", 'none')
    }
      $('.loading-section-two').css("display", 'flex')
    $('#gallery-btn').css("display", 'none')
    setTimeout(addImg, 2000);
  });
  









