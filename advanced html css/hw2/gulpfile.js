import gulp from 'gulp';
import clean from 'gulp-clean';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import prefix from 'gulp-autoprefixer';
import cleanCss from 'gulp-clean-css';
import purgeCss from 'gulp-purgecss';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import imagemin from 'gulp-imagemin';

const sass = gulpSass(dartSass);
import browserSync from 'browser-sync';
browserSync.create();

gulp.task('clean', ()=>{
    return gulp.src('./dist/*', {read: false})
        .pipe(clean());
});

gulp.task('moveCss', ()=>{
    return gulp
        .src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(purgeCss({
            content: ['./*.html']
        }))
        .pipe(prefix({cascade:false}))
        .pipe(cleanCss({compatibility:"ie8"}))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('./dist/css'));
})

gulp.task('moveJs', ()=>{
    return gulp
        .src('./src/**/*.js')
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
})
gulp.task('moveImg', ()=>{
    return gulp
        .src('./src/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'))
})


gulp.task('build', async function (){
    return gulp.series('clean','moveCss', 'moveJs', 'moveImg')()
})

gulp.task('watch', () => {
    gulp.watch('./src/**/*.scss').on('change', gulp.task("moveCss"));
    gulp.watch('./src/**/*.js').on('change', gulp.task("moveJs"));
})

gulp.task('serve', ()=> {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        notify: false,
        online: true
    });
    browserSync.watch('./src', browserSync.reload)
});

gulp.task('dev', gulp.series(
    gulp.parallel('watch', 'serve')
    ))