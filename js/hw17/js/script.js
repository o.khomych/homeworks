'use strict'
/** ## Завдання

Створити об'єкт "студент" та проаналізувати його табель. ' +
'Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

#### Технічні вимоги:
    - Створити порожній об'єкт `student`, з полями `name` та `lastName`.
- Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
- У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel
при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента `tabel`.
- порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення
"Студент переведено на наступний курс".
- Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення `Студенту призначено стипендію`.
 */

const student = {
    name: null,
    lastName: null,
    tabel: {},
    course(){
        let courseLenght = 0;
        for (let key in this.tabel){
            if(this.tabel[key] < 4){
                courseLenght ++;
            }
        }
        if (courseLenght === 0){
            alert("Студент переведено на наступний курс");
        }
    },
    grade(){
        let courseSumGrade = 0;
        let courseLenghtGrade = 0;
        let avarCourseGrade = 0;
        for (let key in this.tabel){
            courseSumGrade += this.tabel[key];
            courseLenghtGrade ++;
            avarCourseGrade = courseSumGrade / courseLenghtGrade;
        }
        if(avarCourseGrade > 7){
            alert(`Студенту призначено стипендію!. Середній бал ${avarCourseGrade}`)
        }
    }
}
let studentName = prompt("What's your name?");
let studentSurname = prompt("What's your surname?");
student.name = studentName;
student.lastName = studentSurname;

while (true){
    let courseName = prompt("Enter course name");
    if (!courseName)    {
        break;
    } else{
        let courseGrade = +prompt("Enter your grade");
        student.tabel[courseName] = courseGrade;
    }
}
console.log(student);
student.course();
student.grade();








