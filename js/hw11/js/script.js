'use strict'
const passForm = document.querySelector('.password-form');
const icon = document.querySelectorAll('i');
const input = document.querySelectorAll('input');
const btn = document.querySelector('.btn')

passForm.addEventListener('click', (element) => {
    let target = element.target.closest('.icon-password');
    if (target){
        icon.forEach(e => {
            if (target.dataset.id === e.dataset.id) e.classList.toggle('active');
        })
        input.forEach(e => {
           if(e.dataset.id === target.dataset.id) e.type = 'text'
        })
    }
})

const firstInput = document.querySelector('#first-pass');
const secondInput = document.querySelector('#second-pass');
const text = document.querySelector('.alert-text')

document.querySelector('form').addEventListener('submit', (event) => {
    if(firstInput.value === secondInput.value && firstInput.value && secondInput.value){
        alert('You are welcome');
        text.textContent = '';
        firstInput.value = '';
        secondInput.value = '';
    } else{
        text.textContent = 'Потрібно ввести однакові значення';
        text.style.cssText= 'color: red; margin-bottom: 10px';

    }
    event.preventDefault()
})
