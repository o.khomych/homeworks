'use strict'
const start = document.querySelector('#start')
const image = document.querySelectorAll(".image-to-show")
const firstImg = image[0];
const lastImg = image[image.length - 1];
const seconds = document.querySelector('#ss')
const mseconds = document.querySelector('#ms')

function switchImg (){
    let visibleImg = document.querySelector('.active');
    if(visibleImg !== lastImg){
        visibleImg.classList.remove('active');
        visibleImg.nextElementSibling.classList.add('active')
    } else {
        visibleImg.classList.remove('active');
        firstImg.classList.add('active')
    }

}
let timer;
const pause = document.createElement('button');
pause.textContent = 'Pause';
let run = 0
start.addEventListener('click', ()=>{
        if(run === 0){
            run = 1
            timerFn();
        }
        timer = setInterval(switchImg, 3000);
        start.after(pause)
        start.disabled = true;
        pause.disabled = false;
})
pause.addEventListener('click', ()=>{
    clearInterval(timer)
    start.disabled = false;
    pause.disabled = true;
    start.textContent = 'Continue'
    if(run === 1){
        seconds.textContent = '03';
        mseconds.textContent = '00'
        t = 0
        timerFn()
        run = 0;
    }
})

let t = 0;
function timerFn (){
    if(run === 1){
        if(t < 0 && seconds.textContent > 0){
            t = 9
            seconds.textContent = '0' + (seconds.textContent - 1)
            timerFn()
        } else if(seconds.textContent === '00' && t < 1){
            t = 9
            seconds.textContent = '02'
            timerFn()

        } else{
            mseconds.textContent === '9' ? mseconds.textContent = t : mseconds.textContent = '0' + t
            setTimeout(timerFn, 100)
            t--
        }
    }
}
