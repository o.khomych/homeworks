1. setTimeout() - виклакає функцію **1 раз** через певний проміжок часу
   setInterval() - виклакає функцію **регулярно** через певний інвервал часу
2. setTimeout() задають із нульовою затримкою для планування виклику функції настільки швидко наскільки це можливо. Але функція буде виконана тільки після виконання поточного коду. Вона можне не спрацювати моментально через наявність у браузерах  свого таймера з мінімально можливою затримкою, і може складати не менше 4 мілісекунд.
3. Для зупинки подальшого виконання функції викликається clearInterval(). 
   Коли  функція  передається в setInterval, на неї створюється внутрішня ссилка і зберігається в планувальнику.
   І для setInterval функція залишається в памяті до тих пір поки не буде викликано clearInterval()