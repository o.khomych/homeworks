'use strict'
const table = document.createElement('table');
document.body.append(table);
table.style.cssText = 'border-collapse: collapse; border-spacing: 0; text-align: center; width: 400px; height: 400px; margin:auto'
const tbody = document.createElement('tbody');
table.appendChild(tbody)

for (let i = 0; i < 8; i++) {
    let tr = document.createElement('tr');
    tbody.append(tr);
    for (let j = 0; j < 8; j++) {
        let td = document.createElement('td');
        tr.append(td);
        td.style.cssText = 'border: 3px solid black; background-color: gray'
    }
}