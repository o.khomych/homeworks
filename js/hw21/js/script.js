'use strict'

const btn = document.querySelector('button');
document.body.style.cssText = 'display: flex; flex-wrap: wrap; min-height: 100vh; align-content: center; justify-content: center'
btn.addEventListener('click', (e)=>{
    btn.remove()
    let input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'Введіть діаметр'
    document.body.append(input)
    let btnDraw = document.createElement('button');
    document.body.appendChild(btnDraw).textContent = 'Намалювати';
    btnDraw.addEventListener('click', ()=> {
        document.body.innerHTML = '';
        circle(input.value)
    })
})

document.body.addEventListener('click', (element) => {
    if (element.target.closest('.crcl')){
        element.target.remove();
    }
})


function circle(dm){
    for (let i = 0; i < 100; i++){
        let circle = document.createElement('div');
        circle.classList.add('crcl')
        document.body.append(circle);
        circle.style.width = circle.style.height = dm + 'px';
        circle.style.borderRadius = '50%';
        circle.style.backgroundColor = getRandomColor();
    }
}

function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);

    return `rgb(${r}, ${g}, ${b})`;
}