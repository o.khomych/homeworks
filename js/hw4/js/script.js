'use strict'

let userFirstNumber = prompt("Enter your first number");
let userSecondNumber = prompt("Enter your second number");

while(isNaN(userFirstNumber) || isNaN(userSecondNumber) || !userFirstNumber || !userSecondNumber){
    userFirstNumber = +prompt("Enter your first number", userFirstNumber);
    userSecondNumber = +prompt("Enter your second number", userSecondNumber);
}

let mathOperator = prompt("Enter operator (ex. +, -, /, *)");
function calc (a, b, c){
    switch (c){
        case "+":
            return +a + +b;
            break;
        case "-":
            return a - b;
            break;
        case "*":
            return a * b;
            break;
        case "/":
            return a / b;
            break;
    }
}
console.log(calc(userFirstNumber, userSecondNumber, mathOperator))