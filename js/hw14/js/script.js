'use strict'

const secondTheme = document.createElement('link');
secondTheme.href = "css/styleTheme2.css"
secondTheme.rel = 'stylesheet'
const themeButton = document.querySelector('.button');
secondTheme.id = 'theme2'

themeButton.addEventListener('click', (e) =>{
        if (document.querySelector('#theme2')){
            secondTheme.remove();
            localStorage.removeItem('theme', secondTheme)
        } else{
            document.head.append(secondTheme);
            localStorage.setItem('theme', secondTheme)
        }
}
)

if (localStorage.getItem('theme')){
    document.head.append(secondTheme);
}
