'use strict'
// пошук числа Фібоначчі з довільними параметрами
const firstNumber = +prompt("Введите первое число последовательности");
const secondNumber = +prompt("Введите второе число последовательности");
const userNumber = +prompt("Введите порядковый номер числа Фибоначчи");

function fib (f0, f1, n){
    if (n > 0){
        for (let i = 2; i <= n; i++){
            let fn = f0 + f1;
            f0 = f1;
            f1 = fn;
        }
        return f1;
    } else {
        for (let i = -2; i >= n; i--){
            let fn = f0 - f1;
            f0 = f1;
            f1 = fn;
        }
        return f1;
    }
}
alert(fib(firstNumber, secondNumber, userNumber));

// пошук класичного числа Фибоначчі
// const userNumber = +prompt("Enter your number");
//
// function fib (n){
//     let f0 = 1;
//     let f1 = 0;
//     if (n >= 0){
//         for (let i = 1; i <= n; i++){
//             let fn = f0 + f1;
//             f0 = f1;
//             f1 = fn;
//         }
//         return f1;
//     } else {
//         for (let i = -1; i >= n; i--){
//             let fn = f0 - f1;
//             f0 = f1;
//             f1 = fn;
//         }
//         return f1;
//     }
// }
// alert(fib(userNumber));