'use strict'
//1
let paragraph = document.querySelectorAll("p");
paragraph.forEach(element => element.style.backgroundColor = "#ff0000");

//2
let idSearch = document.getElementById("optionsList")
console.log(idSearch)
console.log(idSearch.parentElement)
let child = idSearch.childNodes
for (const el of child){
    console.log(el)
}
//3
let testParag = document.querySelectorAll(".testParagraph");
testParag.innerHTML = 'This is a paragraph';
console.log(testParag)

//4
let header = document.querySelector(".main-header")
for (const elem of header.children){
    elem.classList.add('nav-item')
    console.log(elem)
}

//6
let sectionTitle = document.querySelectorAll(".section-title");
for (const element of sectionTitle){
    element.classList.remove('section-title')
    console.log(element)
}