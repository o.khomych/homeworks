'use strict'
const list = document.querySelector('.tabs');
const listContentItem = document.querySelectorAll('.tabs-text');

list.addEventListener('click', function (element){
    let target = element.target.closest('.tabs-title')
    if (target) {
        document.querySelector('.active').classList.remove('active')
        target.classList.add('active');
        listContentItem.forEach(e =>{
            e.classList.remove('active')
            if (e.dataset.name === target.dataset.name){
                e.classList.add('active');
            }
        })
    }
})
