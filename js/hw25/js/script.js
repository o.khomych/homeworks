'use strict'
const input = document.querySelector('input');
const btnNum = document.querySelectorAll('.black');
const btnPink = document.querySelectorAll('.pink');
const calc = document.querySelector('.box');
const memoryIcon = document.querySelector('.memory');
input.value = '0'
let num1 = '';
let num2 = '';
let sign = '';
let result = '';
let finish = false;
let memory = 0
let activeMemory = false
let number = input.value;
calc.addEventListener('click', function (e){
    if (!e.target.classList.contains('button')) return
    input.value = '';
    let key = e.target.value

    if (e.target.closest('.black')){
        if (e.target.value !== 'C'){
            if(num2 === '' && sign === ''){
                if (e.target.value === '.' &&  num1 === ''){
                    num1 = '0.'
                    input.value= num1;
                }
                if (num1.includes('.') && e.target.value === '.'){
                    input.value = num1;
                    return
                }
                if (input.value === '' && e.target.value === '0'){
                    input.value = 0
                    return;
                }
                num1 += key;
                input.value = num1
            } else if (num1 !== '' && num2 !== '' && finish && sign !== ''){
                    num1 = result;
                    num2 += key;
                    input.value = num2
                    finish = false
            } else if ((num1 !== '' && num2 !== '' && finish && sign === '')){
                    num2 = ''
                    num1 = ''
                    finish = false
                    num1 += key
                    input.value = num1
            }
            else {
                if (input.value === '' && e.target.value === '0'){
                    input.value = 0
                    num2 = 0;
                    return;
                }
                if (e.target.value === '.' &&  num2 === ''){
                    num2 = '0.'
                    input.value= num2;
                }
                if (num2.includes('.') && e.target.value === '.'){
                    input.value = num2;
                    return
                }

                num2 += key;
                input.value = num2;
            }
            return
        }
    }
    if (e.target.closest('.pink')){
        if (num1 === '') input.value = 0
        if (num1 !== '' && num2 !== '' && !finish){
            result = calculator(num1, num2, sign);
            input.value = result;
            finish = true
            sign = ''
        } else if(num1 !== '' && num2 === ''){
            sign = key;
            input.value = num1;
            return;
        } else if (num1 === result && finish){
            sign = key;
            num2 = ''
            input.value = num1;
        } 
    }
    if(e.target.closest('.orange')){
        result = calculator(num1, num2, sign);
        num1 = result
        input.value = result;
        finish = true
        sign = ''
    }
    if(e.target.closest('.clear')){
        clear()
    }

    if(e.target.closest('.gray')){
        if (e.target.value === "m-" ){
            if(num1 === ''){
                input.value = '0'
                return;
            }
            else if(finish){
                memory -= Number(result)
                input.value = result
            } else if(!finish && num1 !== '' && num2 === ""){
                memory -= Number(num1)
                input.value = num1
            } else if(!finish && num1 !== '' && num2 !== ""){
                memory -= Number(num2)
                input.value = num2
            }
            memoryIcon.classList.add('active')
        }
        if (e.target.value === "m+" ){
            if(num1 === ''){
                input.value = '0'
                return;
            }
            else if(finish){
                memory += Number(result)
                input.value = result
            } else if(!finish && num1 !== '' && num2 === ""){
                memory += Number(num1)
                input.value = num1
            } else if(!finish && num1 !== '' && num2 !== ""){
                memory += Number(num2)
                input.value = num2
            }
            memoryIcon.classList.add('active')
        }
        if (e.target.value === "mrc"){
            if (!activeMemory){
                if (memory === '') input.value = '0'
                else {
                    input.value = memory;
                    num1 = memory
                }
                activeMemory = true
            } else if(activeMemory){
                memory = 0;
                clear()
                memoryIcon.classList.remove('active')
                activeMemory = false
            }
        }
    }
})

function calculator (a, b, operator){
    // if (b === '') return b = a
    switch (operator){
        case "+":
            return +a + +b;
            break;
        case "-":
            return a - b;
            break;
        case "*":
            return a * b;
            break;
        case "/":
            if (b === 0) {
                a = '';
                b = '';
                operator = ''
                return 'Помилка'
            }
            return a / b
            break;
    }
}

function clear (){
    num1 = '';
    num2 = '';
    sign = '';
    input.value = 0;
    finish = false
}
