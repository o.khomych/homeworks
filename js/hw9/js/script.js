'use strict'

let arr1= ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arr2 = ["1", "2", "3", "sea", "user", 23];
let arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
let arr4 = ["Kharkiv", "Kiev", ["Borispol", "Kharkiv", "Kiev", ["Irpin","Odessa", ["Irpin","Odessa", "Lviv", "Dnieper"], "Lviv", "Dnieper"]], "Odessa", "Lviv", "Dnieper"];

// 1 var
// function showListOnPage (array, domElement = document.body){
//     let list = document.createElement("ul");
//     domElement.append(list);
//     array.forEach((element) => {
//         let listItem = document.createElement('li');
//         listItem.textContent = element;
//         list.append(listItem);
//     })
// }
// showListOnPage(arr1)

// 2 var
function showListOnPage(array, domElement = document.body){
    let list = `<ol>${array.map(function cbFn(item){
        if (!Array.isArray(item)){
            return `<li>${item}</li>`;
        } else{
            return `<ol>${item.map(cbFn)}</ol>`
        }
    })}</ol>`
    let newList = list.split(',').join('')
    domElement.insertAdjacentHTML("beforeend", newList);
    let divTime = document.createElement('div');
    divTime.classList.add('timer');
    domElement.prepend(divTime);
    divTime.style.cssText = `font-size: 64px; position: absolute; right: 40%; top: 20%;`
    let time = 3;
    function timerFn (){
        if (time < 1) {
            domElement.innerHTML = ``;
        } else{
            domElement.querySelector('.timer').innerText = time;
            setTimeout(timerFn, 1000)
            time --;
        }
    }
    timerFn()
}

showListOnPage(arr4);








