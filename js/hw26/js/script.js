'use strict'

const nextBtn = document.querySelector('.next')
const prvBtn = document.querySelector('.prev')
const slider = document.querySelector('.images-wrapper')


function slide(images, next, prev){
    let posInitial;
    const image = images.querySelectorAll('.image-to-show')
    let slidesLength = image.length;
    let slideSize = images.querySelectorAll('.image-to-show')[0].offsetWidth;
    let firstImg = image[0];
    let lastImg = image[slidesLength-1];
    let cloneFirst = firstImg.cloneNode(true)
    let cloneLast = lastImg.cloneNode(true)
    let index = 0
    let allowShift = true;

    images.appendChild(cloneFirst);
    images.insertBefore(cloneLast, firstImg);


    next.addEventListener('click', function () { shiftImg(1) });
    prev.addEventListener('click', function () { shiftImg(-1) });

    images.addEventListener('transitionend', chIndex);


    function shiftImg(k) {
        images.classList.add('shifting');
    if (allowShift){
        posInitial = images.offsetLeft;
        if (k === 1) {
            images.style.left = (posInitial - slideSize) + "px";
            index++;
        } else if (k === -1) {
            images.style.left = (posInitial + slideSize) + "px";
            index--;
        }
    }
    allowShift = false;
}

    function chIndex(){
        images.classList.remove('shifting');

        if (index === -1) {
            images.style.left = -(slidesLength * slideSize) + "px";
            index = slidesLength - 1;
        }
        if (index === slidesLength) {
            images.style.left = -(slideSize) + "px";
            index = 0;
        }
        allowShift = true;
    }


}
slide(slider, nextBtn, prvBtn)



