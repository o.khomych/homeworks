'use strict'

function createNewUser(){
    const newUser = {
        firstName: prompt("Whats your name?"),
        lastName: prompt("Whats your surname?"),
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        setFirstName (newFName){
            Object.defineProperty(this, "firstName",
            {value: newFName})
        },
        setLastName (newLName){
            Object.defineProperty(this, "lastName",
                {value: newLName})
        },
    };
    Object.defineProperties(newUser, {
        "firstName": {
            value: newUser.firstName,
            writable: false,
            configurable: true,
        },
        "lastName":{
            value: newUser.lastName,
            writable: false,
            configurable: true,
        },
    });
    return newUser;
}

let myUser = createNewUser();
console.log(myUser);
console.log(myUser.getLogin());

//Test
// myUser.firstName = "TEST1";
// myUser.lastName = "TEST2";
myUser.setFirstName("TEST1");
myUser.setLastName("TEST2");
console.log(myUser);
