'use strict'
const input = document.createElement('input');
const label = document.createElement('label');
const wrapper = document.createElement('div');
document.body.append(wrapper);
wrapper.append(label);
wrapper.append(input);
input.type = 'Number'
label.textContent = 'Price';
label.style.cssText = 'padding: 10px'

input.addEventListener('focus', function (){
    input.style.cssText = 'border: 2px solid green; outline: 0; outline-offset: 0'
})

let alert = document.createElement('div');
document.body.append(alert);
input.addEventListener('blur', function (e){
    if (input.value <= 0){
        alert.innerText = 'Please enter correct price';
        alert.style.cssText = 'color:red';
        input.style.cssText = 'border: 2px solid red';
    } else{
        let div = document.createElement('div');
        document.body.prepend(div);
        div.classList.add('price-wr')
        div.style.cssText = 'border: 1px solid grey; border-radius: 50px; display: inline-flex; justify-content: space-between; margin:10px; padding: 3px'
        let span = document.createElement('span');
        div.append(span);
        let btn = document.createElement('button');
        btn.textContent = 'x'
        btn.style.cssText = 'border: 1px solid grey; color: grey; outline: 0; outline-offset: 0; width: 16px; height:16px; display: flex; justify-content: center; border-radius: 50%; margin-left: 5px; align-items: center'
        div.append(btn)
        input.style.cssText = 'color: green';
        span.textContent = `Поточна ціна: ${parseInt(input.value)}$`;
        alert.innerText = ''
    }
    document.querySelector('button').addEventListener('click', (e)=>{
        input.value = '';
        e.target.closest('.price-wr').remove();
    })
})
