'use strict'
// ### Дане завдання не обов'язкове для виконання
//
// ## Завдання
//
// Реалізувати функцію, яка дозволить оцінити, чи команда розробників встигне здати проект до настання дедлайну.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
// #### Технічні вимоги:
//     - Функція на вхід приймає три параметри:
//     - масив із чисел, що становить швидкість роботи різних членів команди.
//     Кількість елементів у масиві означає кількість людей у команді.
//     Кожен елемент означає скільки стор поінтів (умовна оцінка складності виконання завдання)
//     може виконати даний розробник за 1 день.
// - масив з чисел, який є беклогом (список всіх завдань, які необхідно виконати).
// Кількість елементів у масиві означає кількість завдань у беклозі.
// Кожне число в масиві означає кількість сторі поінтів, необхідні виконання даного завдання.
// - Дата дедлайну (об'єкт типу Date).
// - Після виконання, функція повинна порахувати, чи команда розробників встигне виконати всі завдання з беклогу
// до настання дедлайну (робота ведеться починаючи з сьогоднішнього дня). Якщо так, вивести на екран повідомлення:
// `Усі завдання будуть успішно виконані за ? днів до настання дедлайну!`. Підставити потрібну кількість днів у текст.
// Якщо ні, вивести повідомлення `Команді розробників доведеться витратити додатково ? годин після дедлайну,
// щоб виконати всі завдання в беклозі`
// - Робота триває по 8 годин на день по будніх днях

// function deadline (speedArray, backlogArray, deadlineDate){
//     let speedPerDay = speedArray.reduce((sum, current) => sum + current, 0)
//     let fullTaskPoint = backlogArray.reduce((sum, current) => sum + current, 0)
//     let daysOfWork = fullTaskPoint / speedPerDay;
//     let curDate = newDate();
//     let count = 0;
//     let day =
// }
//
// let speedArray = [80, 130, 30];
// let backlogArray = [100, 100, 200, 200];
// let deadlineDate = new Date(2022, 10, 15);
// console.log(deadline(speedArray, backlogArray, deadlineDate));


// 1. в день роблять сторі поінтів%  80 + 130 + 30 = 240 сп
//     240 сп - 8 год
//     30 сп за 1 год
// 2. треба виконати всього  100 + 150 + 150 + 200 = 600 сп
//     600/240 = 2,5 днів треба для розязання
// 3. сьогоднішній день 05.08.2022 + 2,5 днів буде = 19.10.2022
// 4. 15.11.2022 > 19.10.2022 - встигаэмо

// let newData = new Date(2022 ,11, 31)

// let day = new Date();
// day.setDate(day.getDate() -1);
// console.log(day)
// let count = 0 ;
// let endDate = "";
// while(count < 7){
//     endDate = new Date(day.setDate(day.getDate() + 1));
//     if (day.getDay() !== 0 && day.getDay() !== 6){
//         count++;
//     }
//
// }
//
// console.log(endDate)


let a = [8, 10, 2];
let b = [100, 200, 10, 480];
let c = new Date(2022, 8, 12);
// let day = new Date();
//
// c.setHours(0, 0, 1, 0)
// day.setHours(0, 0, 1, 0)
//
// //1  знайшли скільки стор. поінтів можна зробити в день
// let speed = a.reduce((sum, current) => sum + current, 0);
// console.log(speed);
//
// //2 знайшли скільки стор.поінтів треба зробити за весь період
// let taskTime = b.reduce((sum, current) => sum + current, 0);
// console.log(taskTime)
//
// // 3 знайшли кількісь днів які потрібні щоб зробити завдання
// let dayQt = taskTime / speed;
// console.log(dayQt)
// console.log(day)
// // 4 знайли дату коли буде зроблене завдання (не враховуючи вихідні)
// let count = 0 ;
// let endDate = "";
// while(count < dayQt){
//     endDate = new Date(day.setDate(day.getDate() + 1));
//     if (day.getDay() !== 0 && day.getDay() !== 6){
//         count++;
//     }
// }
// console.log(endDate)
//
// //5
// let additionalDays;
// if (endDate > c){
//     additionalDays = Math.round((endDate.getTime() - c.getTime()) / (1000*60*60*24)*8);
//     console.log(`Команді розробників доведеться витратити додатково ${additionalDays} годин після дедлайну`);
// } else {
//     additionalDays = Math.round((c.getTime() - endDate.getTime()) / (1000*60*60*24));
//     (additionalDays > 0) ? console.log(`Усі завдання будуть успішно виконані за ${additionalDays} днів до настання дедлайну!`) :
//         console.log(`Усі завдання будуть успішно виконані в день дедлайну!`);
// }


function deadline (speedArray, backlogArray, deadlineDate){
    let day = new Date();
    day.setDate(day.getDate() - 1)
    deadlineDate.setHours(0, 0, 1, 0)
    day.setHours(0, 0, 1, 0)
    let dayQt = backlogArray.reduce((sum, current) => sum + current, 0) / speedArray.reduce((sum, current) => sum + current, 0);
    console.log(dayQt)
    let count = 0 ;
    let endDate = "";
    while(count < dayQt){
        endDate = new Date(day.setDate(day.getDate() + 1));
        if (day.getDay() !== 0 && day.getDay() !== 6){
            count++;
        }
    }
    console.log(endDate)
    let additionalDays;
    if (endDate > deadlineDate){
        additionalDays = Math.round((endDate.getTime() - deadlineDate.getTime()) / (1000*60*60*24));
        let days = 0
        let hours;
        for (let i = 0; i < additionalDays; i++){
            let newDate = new Date(deadlineDate.setDate(deadlineDate.getDate() + 1));
            if (deadlineDate.getDay() !== 0 && deadlineDate.getDay() !== 6){
                days +=1;
                hours = (dayQt -(Math.ceil(dayQt)-days))*8
            }
        }
        console.log(`Команді розробників доведеться витратити додатково ${hours} годин після дедлайну`);
    } else {
        additionalDays = Math.round((deadlineDate.getTime() - endDate.getTime()) / (1000*60*60*24));
        (additionalDays > 0) ? console.log(`Усі завдання будуть успішно виконані за ${additionalDays} днів до настання дедлайну!`) :
            console.log(`Усі завдання будуть успішно виконані в день дедлайну!`);
    }
    console.log(endDate)
}
deadline(a, b, c)