'use strict'

function deadline (speedArray, backlogArray, deadlineDate){
    let day = new Date();
    day.setDate(day.getDate() - 1)
    deadlineDate.setHours(0, 0, 1, 0)
    day.setHours(0, 0, 1, 0)
    let dayQt = backlogArray.reduce((sum, current) => sum + current, 0) / speedArray.reduce((sum, current) => sum + current, 0);
    let count = 0 ;
    let endDate = "";
    while(count < dayQt){
        endDate = new Date(day.setDate(day.getDate() + 1));
        if (day.getDay() !== 0 && day.getDay() !== 6){
            count++;
        }
    }
    let additionalDays;
    if (endDate > deadlineDate){
        additionalDays = Math.round((endDate.getTime() - deadlineDate.getTime()) / (1000*60*60*24));
        let days = 0
        let hours;
        for (let i = 0; i < additionalDays; i++){
            let newDate = new Date(deadlineDate.setDate(deadlineDate.getDate() + 1));
            if (deadlineDate.getDay() !== 0 && deadlineDate.getDay() !== 6){
                days +=1;
                hours = (dayQt -(Math.ceil(dayQt)-days))*8
            }
        }
        console.log(`Команді розробників доведеться витратити додатково ${hours} годин після дедлайну`);
    } else {
        additionalDays = Math.round((deadlineDate.getTime() - endDate.getTime()) / (1000*60*60*24));
        (additionalDays > 0) ? console.log(`Усі завдання будуть успішно виконані за ${additionalDays} днів до настання дедлайну!`) :
            console.log(`Усі завдання будуть успішно виконані в день дедлайну!`);
    }
}

let a = [8, 10, 2];
let b = [100, 200, 10, 480];
let c = new Date(2022, 8, 12);

deadline(a, b, c)