'use strict'
/** ### Дане завдання не обов'язкове для виконання

## Завдання

Реалізувати функцію повного клонування об'єкта. ' +
'Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

#### Технічні вимоги:
    - Написати функцію для  повного рекурсивного клонування об'єкта ' +
'(без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
- Функція повинна успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
- У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або спред-оператор.
 */
const country ={
    Ukraine: {
        cities:{
            Kyiv: {
                population: "3bl",
                area: "800km2",
            },
            Kharkiv: {
                population: "1.5bl",
                area: "500km2",
            },
            Odessa: {
                population: "2.5bl",
                area: "700km2",
            },
        },
        demographics: {
            language: "ukrainian",
            religion: "orthodox",
        },
    },
    Poland: {
        cities:{
            Warsawa: {
                population: "4.8bl",
                area: "760km2",
            },
            Lodz: {
                population: "1.8bl",
                area: "200km2",
            },
            Krakow: {
                population: "2bl",
                area: "790km2",
            },
        },
        demographics: {
            language: "polish",
            religion: "catholic",
        },
    },
}

function clone (object){
    let newObject = {};
    for (let key in object){
        if(typeof object[key] !== "object"){
            newObject[key] = object[key];
        } else{
            newObject[key] = clone(object[key]);
        }
    }
    return newObject;
}

let newObj = clone(country);
console.log(newObj);
