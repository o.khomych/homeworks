'use strict'
//filter
function filterBy(array, type){
    let filterArr = array.filter(item => typeof item !== type);
    return filterArr;
}

console.log(filterBy(['hello', 'world', 23, '23', undefined, false], "string"))


//forEach
function filtersBy(array, type){
    let newArr = [];
    array.forEach(elem => {
        if ((typeof elem !== type)){
            newArr.push(elem)
        }
    })
    return newArr
}

console.log(filtersBy(['hello', 'world', 23, '23', undefined, false], "string"))




