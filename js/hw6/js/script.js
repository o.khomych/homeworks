'use strict'

function createNewUser(){
    const newUser = {
        firstName: prompt("Whats your name?"),
        lastName: prompt("Whats your surname?"),
        birthday: prompt("When were you born? (in format dd.mm.yyyy)"),
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        setFirstName (newFName){
            Object.defineProperty(this, "firstName",
            {value: newFName})
        },
        setLastName (newLName){
            Object.defineProperty(this, "lastName",
                {value: newLName})
        },
        getAge() {
            let birthD = this.birthday.split(".");
            let today = new Date();
            let date = new Date(birthD[2], birthD[1], birthD[0]);
            if (+birthD[1] > (today.getMonth() + 1)){
                date = (today.getFullYear() - date.getFullYear() - 1);
            } else  if (+birthD[1] === (today.getMonth() + 1)){
                if (+birthD[0] <= today.getDate()){
                    date = (today.getFullYear() - date.getFullYear());
                } else {
                    date = (today.getFullYear() - date.getFullYear() - 1);
                }
            }else{
                date = (today.getFullYear() - date.getFullYear());
            }
            return `${date} years old`;
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        },
    };
    Object.defineProperties(newUser, {
        "firstName": {
            value: newUser.firstName,
            writable: false,
            configurable: true,
        },
        "lastName":{
            value: newUser.lastName,
            writable: false,
            configurable: true,
        },
    });
    return newUser;
}

let myUser = createNewUser();
console.log(myUser);
console.log(myUser.getLogin());
console.log(myUser.getAge());
console.log(myUser.getPassword());







