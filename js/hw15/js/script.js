'use strict'
let userNumber;

do {
    userNumber = prompt("Enter your number");
} while (!userNumber || isNaN(userNumber));

function factorial (n){
    if (n === 1){
        return 1;
    } else {
        return n*factorial(n-1);
    }
}
alert(factorial(userNumber));