'use strict'
const table = document.createElement('table');
document.body.append(table);
table.style.cssText = 'border-collapse: collapse; border-spacing: 0; text-align: center; width: 90vw; height: 95vh; margin:auto'
const tbody = document.createElement('tbody');
table.appendChild(tbody)

for (let i = 0; i < 30; i++) {
    let tr = document.createElement('tr');
    tbody.append(tr);
    for (let j = 0; j < 30; j++) {
        let td = document.createElement('td');
        tr.append(td);
        td.style.cssText = 'border: 1px solid black;'
    }
}

table.addEventListener('click', function (event){
    event.target.classList.toggle('colored')
    coloredCells(event.target);
})

document.body.addEventListener('click', function (e){
    if(e.target === e.currentTarget){
        document.querySelectorAll('td').forEach(e => {
            e.classList.toggle('colored');
            coloredCells(e)
        })
    }
})

function coloredCells (element){
    if (element.classList.contains('colored')) {
        element.style.backgroundColor = 'black';
    } else {
        element.style.backgroundColor = '';
    }
}
