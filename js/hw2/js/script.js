'use strict'

let name = prompt("What's your name?");
let age = prompt("How old are you?");

while (!name || isFinite(name) || isNaN(age)){
    name = prompt("What's your name?", name);
    age = +prompt("How old are you?", age);
}

if (age < 18){
    alert("You are not allowed to visit this website.")
} else if (age >= 18 && age <= 22){
    let message = confirm("Are you sure you want to continue?");
    if(message){
            alert(`Welcome, ${name}!`);
        } else{
            alert("You are not allowed to visit this website")
        }
} else{
    alert(`Welcome, ${name}!`);
}