'use strict'
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const container = document.querySelector('#root')

function showOnPage(array, domElement){
    let list = `<ul>${array.map(item => {
        try {
            if (!item.price) {
                throw new Error(`Об'єкт ${array.indexOf(item)+1} Не має властивості price`)
            } else if(!item.name) {
                throw new Error(`Об'єкт ${array.indexOf(item)+1} Не має властивості name`)
            } else if(!item.author) {
                throw new Error(`Об'єкт ${array.indexOf(item)+1} Не має властивості author`)
            } 
            return `<li> ${item.author} ${item.name}  ${item.price} </li>`;
        } catch (err){
            console.log(err.message)
        } 

    })}</ul>`
    let newList = list.split(',').join('')
    return domElement.insertAdjacentHTML("beforeend", newList);
}

showOnPage(books, container)