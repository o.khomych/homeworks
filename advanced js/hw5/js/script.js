'use strict'
const cardContainer =document.querySelector('.card-container')
const modal = document.querySelector('.modal')
const modalChange = document.querySelector('.modal-change')
class Card {
    constructor(title, text, name, surname, email, postsId) {
        this.title = title
        this.text = text
        this.name = name
        this.surname = surname
        this.email = email
        this.postsId = postsId
        this.element = null
    }
    render(){
        return `<div class="card" id="${this.postsId}">
            <div class="card__avatar">
            <svg viewBox="0 0 24 24" aria-hidden="true" class="r-1cvl2hr r-4qtqp9 r-yyyyoo r-16y2uox r-8kz0gk r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-lrsllp"><g><path d="M23.643 4.937c-.835.37-1.732.62-2.675.733.962-.576 1.7-1.49 2.048-2.578-.9.534-1.897.922-2.958 1.13-.85-.904-2.06-1.47-3.4-1.47-2.572 0-4.658 2.086-4.658 4.66 0 .364.042.718.12 1.06-3.873-.195-7.304-2.05-9.602-4.868-.4.69-.63 1.49-.63 2.342 0 1.616.823 3.043 2.072 3.878-.764-.025-1.482-.234-2.11-.583v.06c0 2.257 1.605 4.14 3.737 4.568-.392.106-.803.162-1.227.162-.3 0-.593-.028-.877-.082.593 1.85 2.313 3.198 4.352 3.234-1.595 1.25-3.604 1.995-5.786 1.995-.376 0-.747-.022-1.112-.065 2.062 1.323 4.51 2.093 7.14 2.093 8.57 0 13.255-7.098 13.255-13.254 0-.2-.005-.402-.014-.602.91-.658 1.7-1.477 2.323-2.41z"></path></g></svg>
            </div>
            <div class="card__content">
                <div class="card-header">
                    <div class="card__contacts">
                        <p class="card__fullname">${this.name + this.surname}</p>
                        <p class="card__email">${this.email}</p>
                    </div>
                    <div class="btn-wrapper">
                        <button id="${this.postsId}changeBtn" class="changeButton" >Change</button>
                        <button id="${this.postsId}delBtn" class="deleteButton">Delete</button>
                    </div>
                </div>
                <h2 class="card__title" id="${this.postsId}title">${this.title}</h2>
                <p class="card__text" id="${this.postsId}text">${this.text}</p>
                <div class="card__icons">
                    <svg viewBox="0 0 24 24" aria-hidden="true"
                         class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi">
                        <g>
                            <path
                                d="M1.751 10c0-4.42 3.584-8 8.005-8h4.366c4.49 0 8.129 3.64 8.129 8.13 0 2.96-1.607 5.68-4.196 7.11l-8.054 4.46v-3.69h-.067c-4.49.1-8.183-3.51-8.183-8.01zm8.005-6c-3.317 0-6.005 2.69-6.005 6 0 3.37 2.77 6.08 6.138 6.01l.351-.01h1.761v2.3l5.087-2.81c1.951-1.08 3.163-3.13 3.163-5.36 0-3.39-2.744-6.13-6.129-6.13H9.756z"></path>
                        </g>
                    </svg>
                    <svg viewBox="0 0 24 24" aria-hidden="true"
                         class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi">
                        <g>
                            <path
                                d="M4.5 3.88l4.432 4.14-1.364 1.46L5.5 7.55V16c0 1.1.896 2 2 2H13v2H7.5c-2.209 0-4-1.79-4-4V7.55L1.432 9.48.068 8.02 4.5 3.88zM16.5 6H11V4h5.5c2.209 0 4 1.79 4 4v8.45l2.068-1.93 1.364 1.46-4.432 4.14-4.432-4.14 1.364-1.46 2.068 1.93V8c0-1.1-.896-2-2-2z"></path>
                        </g>
                    </svg>
                    <svg viewBox="0 0 24 24" aria-hidden="true"
                         class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi">
                        <g>
                            <path
                                d="M16.697 5.5c-1.222-.06-2.679.51-3.89 2.16l-.805 1.09-.806-1.09C9.984 6.01 8.526 5.44 7.304 5.5c-1.243.07-2.349.78-2.91 1.91-.552 1.12-.633 2.78.479 4.82 1.074 1.97 3.257 4.27 7.129 6.61 3.87-2.34 6.052-4.64 7.126-6.61 1.111-2.04 1.03-3.7.477-4.82-.561-1.13-1.666-1.84-2.908-1.91zm4.187 7.69c-1.351 2.48-4.001 5.12-8.379 7.67l-.503.3-.504-.3c-4.379-2.55-7.029-5.19-8.382-7.67-1.36-2.5-1.41-4.86-.514-6.67.887-1.79 2.647-2.91 4.601-3.01 1.651-.09 3.368.56 4.798 2.01 1.429-1.45 3.146-2.1 4.796-2.01 1.954.1 3.714 1.22 4.601 3.01.896 1.81.846 4.17-.514 6.67z"></path>
                        </g>
                    </svg>
                    <svg viewBox="0 0 24 24" aria-hidden="true"
                         class="r-4qtqp9 r-yyyyoo r-1xvli5t r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-1hdv0qi">
                        <g>
                            <path
                                d="M12 2.59l5.7 5.7-1.41 1.42L13 6.41V16h-2V6.41l-3.3 3.3-1.41-1.42L12 2.59zM21 15l-.02 3.51c0 1.38-1.12 2.49-2.5 2.49H5.5C4.11 21 3 19.88 3 18.5V15h2v3.5c0 .28.22.5.5.5h12.98c.28 0 .5-.22.5-.5L19 15h2z"></path>
                        </g>
                    </svg>
                </div>
            </div>`
    }
}


async function createPage (){
    const responseUser = await fetch(`https://ajax.test-danit.com/api/json/users`, {method: 'GET'})
    const users = await responseUser.json()

    const responsePosts = await fetch(`https://ajax.test-danit.com/api/json/posts`, {method: 'GET'})
    const posts = await responsePosts.json()

    let userArr = users.sort((a,b)=> b.id - a.id ).map(user =>{
        posts.sort((a,b)=> b.id - a.id ).map(post =>{
            document.querySelector('.loader').style.display='none'
            if (user.id === post.userId) {
                return cardContainer.innerHTML += (new Card(post.title, post.body, user.name, user.username, user.email, post.id)).render()
            }
        })
    })
    return userArr
}

async function deletePost (postId){
    let response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`,{
        method: 'DELETE'
    })
    if (response.status === 200){
        document.getElementById(parseInt(postId)).remove()
    }
}

document.querySelector('#addBtn').addEventListener('click',  async ()=>{
    const uTitle = document.querySelector('#user-title')
    const uText = document.querySelector('#user-text')
    const response = await fetch(`https://ajax.test-danit.com/api/json/posts`,{
        method: 'POST',
        body: JSON.stringify({
            userId: 1,
            title: uTitle.value,
            body: uText.value
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    const res = await response.json()
    console.log(res)
    modal.classList.remove('active')
    uTitle.value = ''
    uText.value= ''
    return cardContainer.insertAdjacentHTML('afterbegin',
        (new Card(res.title, res.body, 'Leanne', 'GrahamBret', 'Sincere@april.biz', res.id)).render())

})
document.body.addEventListener('click', async (e)=>{

    if(e.target.classList.contains('add-btn')){
        modal.classList.add('active')
    }
    if(e.target.classList.contains('close')){
        modal.classList.remove('active')
        modalChange.classList.remove('active')
    }
    if (e.target.classList.contains('deleteButton')){
        await deletePost(parseInt(e.target.id))
    }

    if(e.target.classList.contains('changeButton')){
        let id = parseInt(e.target.id)
        let modalChangeTitle = document.querySelector('#change-title')
        let modalChangeText = document.querySelector('#change-text')
        modalChange.classList.add('active')
        modalChangeTitle.value = document.getElementById(id+'title').textContent
        modalChangeText.value = document.getElementById(id+'text').textContent

        document.querySelector('.saveBtn').addEventListener('click', async ()=>{
            if (document.getElementById(id)) {
                document.getElementById(id + 'title').textContent = modalChangeTitle.value
                document.getElementById(id + 'text').textContent = modalChangeText.value
                await changePost(id, modalChangeTitle.value, modalChangeText.value)
                document.querySelector('.close-modal-change').addEventListener('click', () => {
                    modalChange.classList.remove('active')
                    id = ''
                })
            }
        })
    }
})



async function changePost (postId, changeTitle, changeBody){
    let response = await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'PUT',
        body: JSON.stringify({
            id: postId,
            title: changeTitle,
            body: changeBody
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
    let res = await response.json()
    console.log(res)
}


(async()=>{
    await createPage()
})()
