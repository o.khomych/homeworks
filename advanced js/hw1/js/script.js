'use strict'

class Employee {
    constructor(name, age, salary) {
        this.name = name
        this.age = age
        this.salary = salary
    }
    set name(value){
        this._name = value
    }
    get name(){
        return this._name
    }
    set age(value){
        this._age = value
    }
    get age(){
        return this._age
    }
    set salary(value){
        this._salary = value
    }
    get salary(){
        return this._salary
    }

}
 class Programmer extends Employee{
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang
    }
     set salary(value){
         this._salary = value * 3
     }
     get salary(){
         return this._salary
     }
 }

const programer1 = new Programmer("Alex", 38, 1000, 'ua');
console.log(programer1);

const programer2 = new Programmer("Dina", 25, 1500, 'en');
console.log(programer2);

const programer3 = new Programmer("Alex", 30, 2000, 'en');
console.log(programer3);