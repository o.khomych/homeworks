'use strict'

async function findIp (){
    const btn = document.querySelector('#btn')
    let ipResponse = await fetch(`https://api.ipify.org/?format=json`)
    let ipResult = await ipResponse.json()
    let ip = ipResult.ip

    let addressResponse = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district`)
    let addressResult = await addressResponse.json()

    let result = `<div>
            <p>Continent: ${addressResult.continent}</p>
            <p>Country: ${addressResult.country}</p>
            <p>Region: ${addressResult.regionName}</p>
            <p>City: ${addressResult.city}</p>
            <p>District: ${addressResult.district}</p>
    </div>`

    btn.addEventListener('click',  ()=>{
        btn.insertAdjacentHTML('afterend', result)
    })
}

findIp()

