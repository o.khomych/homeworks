

// let response = () => {
//
// }


    fetch(`https://ajax.test-danit.com/api/swapi/films`)
        .then(res => res.json())
        .then(result => {
            let filmList =
              `<ul>${result.map((el) => {
                  return `<li>
                          <h2>Episode ${el.episodeId}</h2>   
                          <h3>${el.name} </h3> 
                          <p>${el.openingCrawl}</p>
                          <ul id='${el.id}'></ul>
                          <p class="loader"></p>
                         </li>`
              })} </ul> `
            document.body.innerHTML = filmList.split(',').join('')
            return result
        }
        )
        .then(result => {
            return result.map(charList =>{
                charList.characters.map(link =>{
                    fetch(link).then(res=> res.json()).then(character => {
                        document.querySelectorAll('.loader')[charList.id-1].style.display = 'none'
                        document.getElementById(charList.id).innerHTML += `<li>${character.name}</li>`
                    })
                })
            })
        })

